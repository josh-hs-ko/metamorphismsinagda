MetamorphismsInAgda: MetamorphismsInAgda.tex
	pdflatex MetamorphismsInAgda

MetamorphismsInAgda.tex: MetamorphismsInAgda.lhs
	lhs2TeX --agda MetamorphismsInAgda.lhs -o MetamorphismsInAgda.tex
#	sed -i '' '/inputenc/d' MetamorphismsInAgda.tex
