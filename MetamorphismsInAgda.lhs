% -*- coding: utf-8; -*-
% vim: set fileencoding=utf-8 :
\documentclass[english,crc]{programming}
%% First parameter: the language is 'english'.
%% Second parameter: use 'submission' for initial submission, remove it for camera-ready (see 5.1)

%include agda.fmt

%subst keyword a = "\keyword{" a "}"
%subst conid a = "\identifier{" a "}"
%subst varid a = "\identifier{" a "}"
%subst numeral a = "\mathbf{" a "}"
%format : = "{:}"
%format LETEQ = "{=}"
%format Set = "\constructor{Set}"
%format DOT = ".\kern-3pt"
%format (GOAL(t)(i)) = "\highlight{goal}{\textbf\{\," t "\,\textbf\}_{\kern1pt" i "}}"
%format (GOAL'(t)) = "\highlight{goal}{\textbf\{\," t "\,\textbf\}}"
%format (CXT(t)) = "\kern-2pt_{\highlight{cxt}{" t "}}"
%format constant ="\keyword{constant}"

%format ◁ = "{\kern-.25pt\lhd\kern.25pt}"
%format _◁_ = _ ◁ _
%format ▷ = "{\kern.5pt\rhd\kern-.5pt}"
%format _▷_ = _ ▷ _
%format Rational = "\mathbb{Q}"
%format Nat = "\mathbb{N}"
%format S_C = "S_\mathrm{\kern.5ptC}"
%format ▷ᶜ = "\kern.5pt{\rhd}_\mathrm{C}\kern-.5pt"
%format _▷ᶜ_ = _ ▷ᶜ _
%format g_C = "g_\mathrm{\kern.5ptC}"
%format g_C' = "g_\mathrm{\kern.5ptC}^\prime"
%format e_C = "\identifier{e}_\mathrm{C}"
%format if = "\keyword{if}"
%format then = "\keyword{then}"
%format else = "\keyword{else}"
%format Val = "\constructor{Val}"
%format Heap = "\constructor{Heap}"
%format _≡_ = _ "\kern.5pt" ≡ _
%format refl = "\constructor{refl}"
%format × = "{×}"
%format , = "\kern-1pt,"
%format inj₁ = "\constructor{inj_1}"
%format inj₂ = "\constructor{inj_2}"
%format Maybe = "\constructor{Maybe}"
%format nothing = "\constructor{nothing}"
%format just = "\constructor{just}"
%format List = "\constructor{List}"
%format AlgList = "\constructor{AlgList}"
%format Vec = "\constructor{Vec}"
%format CoList = "\constructor{CoList}"
%format CoListF = "\constructor{CoListF}"
%format CoalgList = "\constructor{CoalgList}"
%format CoalgListF = "\constructor{CoalgListF}"
%format [] = "[\kern1pt]"
%format ∷ = "{::}"
%format ∷⟨ = ∷ ⟨
%format _∷⟨_⟩_ = _ "\kern1pt" ∷⟨ "\kern-1pt" _ ⟩ _
%format ⟨_⟩ = ⟨ "\kern-1pt" _ ⟩
%format _∷_ = _ "\kern1pt" ∷ _
%format [ = "[\!"
%format ] = "\!]"
%format decon = "\constructor{decon}"
%format Σ[ = "\Sigma" [
%format jigsawᵢᵥ = "\identifier{jigsaw_\mathrm{\,IV}}"
%format fillᵢᵥ = "\identifier{fill_\mathrm{\,IV}}"
%format jigsaw-conditionᵢ = "\identifier{jigsaw-condition_\mathrm{\,I}}"
%format jigsawᵢₕ = "\identifier{jigsaw_\mathrm{\,IH}}"
%format fillᵢₕ = "\identifier{fill_\mathrm{\,IH}}"
%format INF = "\infty"
%format g∞ = "\identifier{g}^\infty"
%format flat? = "\identifier{flat}^?"

%format b_i = "b_i"
%format b_o = "b_o"
%format w_i = "w_i"
%format w_o = "w_o"

\usepackage[backend=bibtex]{biblatex}
\addbibresource{bib.bib}
\usepackage{newunicodechar}
\newunicodechar{∘}{\ensuremath{\mathrel\circ}}
\newunicodechar{×}{\ensuremath{\times}}
\newunicodechar{⟨}{\ensuremath{\langle}}
\newunicodechar{⟩}{\ensuremath{\rangle}}
\newunicodechar{⊎}{\ensuremath{\uplus}}
\newunicodechar{≢}{\ensuremath{\not\equiv}}
\newunicodechar{ℕ}{\ensuremath{\mathbb{N}}}

\newcommand{\textequivalence}{\ensuremath{\equiv}}
\newcommand{\textelement}{\ensuremath{\in}}

\usepackage{xcolor}
\definecolor{goal}{RGB}{209,243,205}
\definecolor{cxt}{RGB}{255,255,150}
\definecolor{emphasis}{RGB}{255,255,200}

\newcommand{\token}[1]{{\operatorname{\mathcode`\'="8000 #1}}}
\newcommand{\keyword}[1]{\token{\mathbf{#1}}}
\newcommand{\identifier}[1]{\token{\mathit{#1}}}
\newcommand{\constructor}[1]{\token{\mathsf{#1}}}

\newcommand{\highlight}[2]{\smash{\text{\colorbox{#1}{\kern-.1em\vphantom{\vrule height 1.2ex depth 0.1ex}\smash{\ensuremath{#2}}\kern-.1em}}}}

\makeatletter
\newcommand{\shorteq}{%
  \settowidth{\@@tempdima}{-}%
  \resizebox{\@@tempdima}{\height}{=}%
}
\makeatother

%\newcommand{\varref}[2]{\hyperref[#2]{#1~\ref*{#2}}}

\usepackage{etoolbox}
\usepackage{nicefrac}

\usepackage{tikz}
\usetikzlibrary{matrix,arrows,calc,fadings,positioning}
\tikzset{every path/.append style={semithick}}
\begin{tikzfadingfrompicture}[name=fade out, x=25bp, y=25bp]
\fill[color=transparent!0] (-1, -.66) rectangle (1, .66);
\shade[top color=transparent!0, bottom color=transparent!100] (-1, -1) rectangle (1, -.4);
\shade[top color=transparent!100, bottom color=transparent!0] (-1, .4) rectangle (1, 1);
\end{tikzfadingfrompicture}
\tikzset{label on arrow/.style={fill=white, path fading=fade out}}
\begin{tikzfadingfrompicture}[name=horizontal fade out, x=25bp, y=25bp]
\fill[color=transparent!0] (-.66 , -1) rectangle (.66 , 1);
\shade[right color=transparent!0, left color=transparent!100] (-1, -1) rectangle (-.65 , 1);
\shade[right color=transparent!100, left color=transparent!0] (.65, -1) rectangle (1, 1);
\end{tikzfadingfrompicture}
\tikzset{horizontal label on arrow/.style={fill=white, path fading=horizontal fade out}}

\usepackage[color=yellow,textsize=scriptsize]{todonotes}
%\setlength{\marginparwidth}{1cm}

%\newcommand{\varparagraph}[1]{\par\ensuremath{\blacktriangleright}~\textbf{#1}\startblock}
%\newcommand{\startblock}{\hspace{.75em}}
\newcommand{\varparagraph}{\paragraph}
\newcommand{\awa}[2]{\mathrlap{#2}\phantom{#1}} % as wide as
\newcommand{\varawa}[2]{\phantom{#1}\mathllap{#2}}
%\newcommand{\varcitet}[3][]{\cite{#2}#3~\ifthenelse{\isempty{#1}}{\citeyearpar{#2}}{\citeyearpar[#1]{#2}}}

%%%%%%%%%%%%%%%%%%
%% These data are provided by the editors. May be left out on submission.
\paperdetails{
  submitted=2020-04-29,
  published=2020-11-01,
  year=2021,
  volume=5,
  issue=2,
  articlenumber=7,
}
%%%%%%%%%%%%%%%%%%

\begin{document}

\setlength{\mathindent}{\parindent}

\newcommand{\beforefigurecode}{\setlength{\mathindent}{0em}\small}

\title{Programming Metamorphic Algorithms}
\subtitle{An Experiment in Type-Driven Algorithm Design}

\author{Hsiang-Shang Ko}
\affiliation{Institute of Information Science, Academia Sinica, Taiwan}
\authorinfo[figs/photo.jpg]{is an Assistant Research Fellow at the Institute of Information Science, Academia Sinica, Taiwan. Contact him at \email{joshko@@iis.sinica.edu.tw}.}

\keywords{dependent types, interactive type-driven development, metamorphisms, Agda} % please provide 1--5 keywords

%%%%%%%%%%%%%%%%%%
%% These data MUST be filled for your submission. (see 5.3)
\paperdetails{
  %% perspective options are: art, sciencetheoretical, scienceempirical, engineering.
  %% Choose exactly the one that best describes this work. (see 2.1)
  perspective=art,
  %% State one or more areas, separated by a comma. (see 2.2)
  %% Please see list of areas in http://programming-journal.org/cfp/
  %% The list is open-ended, so use other areas if yours is/are not listed.
  area={Dependent Types, Functional Programming, Programming Environments},
  %% You may choose the license for your paper (see 3.)
  %% License options include: cc-by (default), cc-by-nc
  % license=cc-by,
}
%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Please go to https://dl.acm.org/ccs/ccs.cfm and generate your Classification
% System [view CCS TeX Code] stanz and copy _all of it_ to this place.
%% From HERE
\begin{CCSXML}
<ccs2012>
   <concept>
       <concept_id>10011007.10011074.10011092</concept_id>
       <concept_desc>Software and its engineering~Software development techniques</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
   <concept>
       <concept_id>10011007.10011006.10011008.10011009.10011012</concept_id>
       <concept_desc>Software and its engineering~Functional languages</concept_desc>
       <concept_significance>500</concept_significance>
       </concept>
 </ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Software development techniques}
\ccsdesc[500]{Software and its engineering~Functional languages}
% To HERE
%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

\begin{abstract}

In \emph{dependently typed programming}, proofs of basic, structural properties can be embedded implicitly into programs and do not need to be written explicitly.
Besides saving the effort of writing separate proofs, a most distinguishing and fascinating aspect of dependently typed programming is that it makes the idea of \emph{interactive type-driven development} much more powerful, where expressive type information becomes useful hints that help the programmer to complete a program.
There have not been many attempts at exploiting the full potential of the idea, though.
As a departure from the usual properties dealt with in dependently typed programming, and as a demonstration that the idea of interactive type-driven development has more potential to be discovered, we conduct an experiment in `type-driven algorithm design': we develop algorithms from their specifications encoded in sophisticated types, to see how useful the hints provided by a type-aware interactive development environment can be.
The algorithmic problem we choose is \emph{metamorphisms}, whose definitional behaviour is consuming a data structure to compute an intermediate value and then producing a codata structure from that value, but there are other ways to compute metamorphisms.
We develop Gibbons's streaming algorithm and Nakano's jigsaw model in the interactive development environment provided by the dependently typed language Agda, turning intuitive ideas about these algorithms into formal conditions and programs that are correct by construction.

\end{abstract}

\section{Interactive Type-Driven Development}
\label{sec:introduction}

%Why do we want to program with full dependent types?
%For larger-scale proofs, writing proof terms in a dependently typed language is usually less efficient than working with a proof assistant with decent automation.
With the use of indexed (co-)datatypes~\cite{Dybjer-inductive-families, Thibodeau-indexed-codata-types}, \emph{dependently typed programming}~\cite{McBride-Agda-curious, McKinna-why-dependent-types-matter} has been successful in offering `intrinsic' guarantees about basic properties whose proofs closely follow program structures and can be implicitly embedded into programs.
Typical examples include keeping indices within bounds~\cite{Innes-tic-tac-types}, representing well-scoped and well-typed syntax trees of embedded languages~\cite{Allais-binding-syntax-universe}, and tracking the available effects/resources used in programs~\cite{Brady-algebraic-effects}.
All these examples involve some form of constraints: bounds, typing, or available resources.
In dependently typed programming, these constraints are embedded into the respective datatypes (of indices, syntax trees, and program operations) so that the type system can help the programmer to enforce the constraints without writing additional proofs.
But this kind of guarantee is also achievable `extrinsically' with proof assistants, where proofs are written explicitly and separately from programs --- we know how to prove theorems saying that indices are always within bounds or that a program transformation always produces well-typed programs, and it is not hard to prove these theorems formally, especially with proof assistants that have decent automation~\cite{Bertot-Coq, Nipkow-IsabelleHOL}.
What distinguishes dependently typed programming from other methodologies is that it makes the idea of \emph{interactive type-driven development} much more powerful: the more properties we encode intrinsically into types, the more hints the type-checker can offer the programmer during an interactive development process, so that the more heavyweight typing becomes an aid rather than a burden~\cite{Ko-OrnJFP, McBride-Epigram, McBride-pivotal}.
%A typical example is making the terms of an embedded domain-specific language (DSL) intrinsically typed, so that we are always aware of the precise types of the terms (in the DSL's type system) and guided to construct only well-typed terms.

A natural follow-up question is: how well does interactive type-driven development scale?
For instance, if we encode a complete specification of an algorithmic problem in a type, will the hints provided by the type-checker be useful enough to help us with the formal construction of an algorithm (about which we may already have an intuitive idea but have not worked out the formal detail)?
Thankfully, we now have dependently typed languages like Agda~\cite{Kokke-PLFA, Norell-thesis, Stump-Agda-book} and Idris~\cite{Brady-Idris, Brady-Idris-book} whose interactive development environment is mature enough for doing some experiments --- that is, trying to program some algorithms interactively with the type-checker.
In this paper we will use Agda~2.6.1 with Standard Library version~1.3.
The code in this paper is available~\cite{Ko-Code}.

The ideal reader of this paper is someone who has experience in Agda programming (for example, who has worked through Part~1 of \textit{Programming Language Foundations in Agda}~\cite{Kokke-PLFA}) and wishes to see some algorithmic examples.
On the other hand, for a reader with some basic knowledge about functional programming and dependent types (e.g., dependent function and pair types and indexed datatypes), the paper will provide necessary explanations about Agda-specific details so that the reader can at least get a sense of what it feels like to program interactively in Agda.

To make our experiment meaningful and more likely to succeed, we had better choose an algorithmic problem that is not too trivial and also not too complicated.
One such problem that comes to mind is metamorphisms.

\section{Metamorphisms}
\label{sec:metamorphisms}

A \emph{metamorphism}~\cite{Gibbons-metamorphisms} consumes a data structure to compute an intermediate value and then produces a new data structure using the intermediate value as the seed.
Following Gibbons~\cite{Gibbons-metamorphisms}, we will focus on \emph{list metamorphisms}, i.e., metamorphisms consuming and producing lists.
Two of the examples of list metamorphisms given by Gibbons are
\begin{itemize}
\item \emph{Base conversion for fractions}: A list of digits representing a fractional number in a particular base (e.g., $0.625_{10}$) can be converted to another list of digits in a different base (e.g., $0.101_2$).
The conversion is a metamorphism because we can consume an input list of digits to compute the value represented by the list, and then produce an output list of digits representing the same value.
Note that even when the input list is finite, the output list may have to be infinite --- for example, $0.1_3 = 0.333\ldots_{10}$.
\item \emph{Heapsort}: An input list of numbers is consumed by pushing every element into a min-heap (priority queue), from which we then pop all the elements, from the smallest to the largest, producing a sorted output list.
\end{itemize}
We will use these two examples to provide some intuition about metamorphisms, but the examples will not be our focus.
Instead, we will mainly be working on a number of generic algorithms that compute metamorphisms~\cite{Gibbons-metamorphisms, Nakano-jigsaw}.
Knowing the existence of these algorithms, our experiment will have a clearer direction to follow and be more likely to succeed.
However, rather than merely implementing them in Agda, we should try to reinvent as much of the algorithms as possible, so that we can feel what it is like to get from some algorithmic intuition and a specification to a formal algorithm with the help of Agda, and perhaps gain a different and/or better understanding of these algorithms.

Let us first say more precisely what metamorphisms are.
Formally, a metamorphism is a \emph{fold} followed by an \emph{unfold}, the former consuming a finite data structure and the latter producing a potentially infinite codata structure.

\varparagraph{Lists for consumption}
For list metamorphisms, the inputs to be consumed are the standard finite lists:%
\footnote{In Agda, a name with underscores like~|_∷_| can be used as an operator, and the underscores indicate where the arguments go.
However, some binary infix operators like~|_◁_| are written in Haskell syntax like |(◁)| (which is not syntactically correct in Agda).
All subsequent footnotes will be about Agda-specific details, and can be safely skipped by Agda programmers.}
\begin{code}
data List (A : Set) : Set where
  []   : List A
  _∷_  : A -> List A -> List A
\end{code}
The |foldr| operator subsumes the elements (of type~|A|) of a list into a state (of type~|S|) using a `right algebra' |(◁) : A -> S -> S| and an initial/empty state |e : S|:%
\footnote{In the type of a function, arguments wrapped in curly brackets are implicit, and can be left out (if they are inferable by Agda) when applying the function.

Agda provides some ways to shorten type signatures.
For one, named arguments of the same type can be declared together: here, for example, `|{A S : Set} ->|' is an abbreviation of `|{A : Set} -> {S : Set} ->|'.
Also, in the latter type fragment, the arrow between typed arguments can be skipped, resulting in another valid form `|{A : Set} {S : Set} ->|'.}
\begin{code}
foldr : {A S : Set} -> (A -> S -> S) -> S -> List A -> S
foldr (◁) e []        = e
foldr (◁) e (a ∷ as)  = a ◁ foldr f e as
\end{code}
With |foldr|, a list is consumed from the right.
Dually, the |foldl| operator consumes a list from the left using a `left algebra' |(▷) : S -> A -> S|:
\begin{code}
foldl : {A S : Set} -> (S -> A -> S) -> S -> List A -> S
foldl (▷) e []        = e
foldl (▷) e (a ∷ as)  = foldl (▷) (e ▷ a) as
\end{code}
A list metamorphism can use either |foldr| or |foldl| in its consuming part, and we will see both kinds in the paper.
We will refer to a list metamorphism using |foldr| as a `right metamorphism', and one using |foldl| as a `left metamorphism'.

\varparagraph{Colists for production}
For the producing part of list metamorphisms, where we need to produce potentially infinite lists, in a total language like Agda we can no longer use |List|, whose elements are necessarily finite; instead, we should switch to a \emph{codatatype} of \emph{colists}, which are potentially infinite.
Dual to a datatype, which is defined by all the possible ways to \emph{construct} its elements, a codatatype is defined by all the possible ways to \emph{deconstruct} or \emph{observe} its elements.
For a colist, only one observation is possible: exposing the colist's outermost structure, which is either empty or a pair of a head element and a tail colist.
In Agda this can be expressed as a coinductive record type |CoList| with only one field, which is a sum structure that is either empty or non-empty; for cosmetic reasons, this sum structure is defined (mutually recursively with |CoList|) as a datatype |CoListF|:%
\footnote{Agda supports constructor overloading, so we are allowed to reuse |[]| and |_∷_| as the names of the constructors of |CoListF|.}
\begin{code}
mutual

  record CoList (B : Set) : Set where
    coinductive
    field
      decon : CoListF B

  data CoListF (B : Set) : Set where
    []   : CoListF B
    _∷_  : B -> CoList B -> CoListF B

open CoList
\end{code}
Note that |decon| denotes a function of type |{B : Set} -> CoList B -> CoListF B|, and plays the role of the deconstructor of |CoList|.
Now we can define the standard |unfoldr| operator, which uses a coalgebra |g : S -> Maybe (B × S)| to unfold a colist from a given state:%
\footnote{The datatype |Maybe X| has two constructors, |nothing : Maybe X| and |just : X -> Maybe X|.}
\begin{code}
unfoldr : {B S : Set} -> (S -> Maybe (B × S)) -> S -> CoList B
decon (unfoldr g s) with g s
decon (unfoldr g s) | nothing        = []
decon (unfoldr g s) | just (b , s')  = b ∷ unfoldr g s'
\end{code}
The operator is defined with copattern matching~\cite{Abel-copatterns}:
To define |unfoldr g s|, which is a colist, we need to specify what will be produced if we deconstruct it, that is, the result of |decon (unfoldr g s)|.
This result depends on whether |g|~can produce anything from~|s|, so, using the |with| construct, we introduce |g s| as an additional `argument', on which we can then perform a pattern match.
If |g s| is |nothing|, then the resulting colist will be empty --- that is, |decon (unfoldr g s)| will compute to |[]|;
otherwise, |g s| is |just (b , s')| for some |b|~and~|s'|, and the resulting colist will have |b|~as its head and |unfoldr g s'| as its tail --- that is, |decon (unfoldr g s)| will compute to |b ∷ unfoldr g s'|.

To be more concrete, let us describe our two examples --- base conversion for fractions and heapsort --- explicitly as metamorphisms.
We look at heapsort first as it is simpler.

\varparagraph{Heapsort}
Let |Heap| be an abstract type of min-heaps on some totally ordered set |Val|, equipped with three operations
\begin{code}
empty   : Heap
push    : Val -> Heap -> Heap
popMin  : Heap -> Maybe (Val × Heap)
\end{code}
where |empty| is the empty heap, |push| adds an element into a heap, and |popMin| returns a minimum element and the rest of the input heap if and only if the input heap is non-empty.
Then heapsort can be directly described as a right metamorphism:
\begin{code}
unfoldr popMin ∘ foldr push empty
\end{code}
(In this case the length of the output colist is always finite.
This fact is not captured by the type, which, however, should cover other cases where the length of the output colist can be infinite, such as base conversion for fractions.)

\varparagraph{Base conversion for fractions}
Suppose that the input and output bases are |b_i : Nat| and |b_o : Nat| --- in $0.625_{10} = 0.101_2$, for example, $b_i = 10$ and $b_o = 2$.
We represent fractions as (co)lists of digits (of type~|Nat|) starting from the most significant digit --- for example, $0.625$ is represented as |{-"6\;"-} ∷ {-"2\;"-} ∷ {-"5\;"-} ∷ []|.
Gibbons~\cite[Section~4.2]{Gibbons-metamorphisms} gives a more complete story, where base conversion for fractions is first described as a right metamorphism with simple states (consisting of only an accumulator), and then transformed to a left metamorphism with more complex states.
To make the story short here, we go directly for a left metamorphism
\begin{code}
unfoldr g_C ∘ foldl (▷ᶜ) e_C
\end{code}
where the state type is |S_C LETEQ Rational × Rational × Rational|, which are triples of rationals of the form |(v , w_i , w_o)| where |v|~is an accumulator, |w_i| the weight of the incoming input digit, and |w_o| the weight of the outgoing output digit.
The initial state |e_C| is |({-"0\;"-} , {-"\nicefrac{1}{\identifier{b_i}}\;"-} , {-"\;\nicefrac{1}{\identifier{b_o}}"-})|.
The left algebra~|(▷ᶜ)| adds the product of the current input digit and its weight to the accumulator, and updates the input weight in preparation for the next input digit:
\begin{code}
(▷ᶜ) : S_C -> Nat -> S_C
(v , w_i , w_o ) ▷ᶜ d = ({-"\identifier{v} + d \times w_i\;"-} , {-"\nicefrac{w_i}{b_i}\;"-}, w_o )
\end{code}
On the other hand, the coalgebra~|g_C| produces an output digit and updates the accumulator and the next output weight if the accumulator is not yet zero:
\begin{code}
g_C : S_C -> Maybe (Nat × S_C)
g_C (v , w_i , w_o) = if {-"\identifier{v} > 0\;"-}  then  let  d  LETEQ {-"\;\lfloor\nicefrac{\identifier{v}\kern1pt}{\identifier{w_o}}\rfloor"-}; r  LETEQ {-"\;\identifier{v} - d \times \identifier{w_o}"-}
                                                           in   just (d , (r , w_i , {-"\;\nicefrac{\identifier{w_o}\kern1pt}{\identifier{b_o}}"-}))
                                                     else  nothing
\end{code}
For the example $0.625_{10} = 0.101_2$, the metamorphism first consumes the input digits using~|(▷ᶜ)|,
\[ (0\,,\;0.1\,,\;0.5) ~\stackrel{6}{\mapsto}~ (0.6\,,\;0.01\,,\;0.5) ~\stackrel{2}{\mapsto}~ (0.62\,,\;0.001\,,\;0.5) ~\stackrel{5}{\mapsto}~ (0.625\,,\;0.0001\,,\;0.5) \]%
and then produces the output digits using~|g_C|:
\begin{align*}
(0.625\,,\;0.0001\,,\;0.5) ~\stackrel{1}{\mapsto}~ (0.125\,,\;0.0001\,,\;0.25) ~&\stackrel{0}{\mapsto}~ (0.125\,,\;0.0001\,,\;0.125) \\
&\stackrel{1}{\mapsto}~ (0\,,\;0.0001\,,\;0.0625) ~\not\mapsto
\end{align*}

%\begin{alignat*}{5}
%   & |foldl|\ |(▷ᶜ)|\ &&e_C && && && [6, 2, 5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ &&(0, && 0.1, && 0.5) && [6, 2, 5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ (&&(0, && 0.1, && 0.5) \mathop{|▷ᶜ|} 6)\ && [2, 5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ &&(0.6, && 0.01, && 0.5) && [2, 5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ (&&(0.6, && 0.01, && 0.5) \mathop{|▷ᶜ|} 2) && [5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ &&(0.62, && 0.001, && 0.5) && [5] \\
%=~~& |foldl|\ |(▷ᶜ)|\ (&&(0.62, && 0.001, && 0.5) \mathop{|▷ᶜ|} 5) && [] \\
%=~~& |foldl|\ |(▷ᶜ)|\ &&(0.625, && 0.0001, && 0.5) && [] \\
%=~~& &&(0.625,\ && 0.0001,\ && 0.5)
%\end{alignat*}

\section{Specification of Metamorphisms in Types}
\label{sec:specification}

In the rest of this paper we will develop (actually, reinvent) several \emph{metamorphic algorithms}, which compute a metamorphism but do not take the form of a fold followed by an unfold.
Rather than proving extrinsically that these algorithms satisfy their metamorphic specifications, we will encode metamorphic specifications intrinsically in types, such that any type-checked program is a correct metamorphic algorithm.

\varparagraph{Algebraic ornamentation}
The encoding is based on McBride's \emph{algebraic ornamentation}~\cite{McBride-ornaments}.
Given a right algebra |(◁) : A -> S -> S| and |e : S|, we can refine |List A| into a family of types |AlgList A (◁) e : S -> Set| (for `algebraic lists') indexed by~|S| such that (conceptually) every list~|as| falls into the type |AlgList A (◁) e (foldr (◁) e as)|.
The definition of |AlgList| is obtained by `fusing' |foldr| into |List|:
\begin{code}
data AlgList (A : Set) {S : Set} ((◁) : A -> S -> S) (e : S) : S -> Set where
  []   : AlgList A (◁) e e
  _∷_  : (a : A) -> {s : S} -> AlgList A (◁) e s -> AlgList A (◁) e (a ◁ s)
\end{code}
The empty list is classified under the index |e LETEQ foldr (◁) e []|.
For the cons case, if a tail~|as| is classified under~|s|, meaning that |foldr (◁) e as LETEQ s|, then the whole list |a ∷ as| should be classified under |a ◁ s| since |foldr (◁) e (a ∷ as) LETEQ a ◁ foldr (◁) e as LETEQ a ◁ s|.
For example, we can obtain vectors, or length-indexed lists,
\begin{code}
data Vec (A : Set) : ℕ -> Set where
  []   : Vec A {-"\;0"-}
  _∷_  : (a : A) -> {n : ℕ} -> Vec A n -> Vec A ({-"1 + n"-})
\end{code}
simply by defining |Vec A = AlgList A (\ _ n -> {-"\;1 + n"-}) {-"\;0"-}|, since |length = foldr (\ _ n -> {-"\;1 + n"-}) {-"\;0"-}|.

\varparagraph{Coalgebraic ornamentation}
Dually, given a coalgebra |g : S -> Maybe (B × S)|, we can refine |CoList B| into a family of types |CoalgList B g : S -> Set| (for `coalgebraic (co)lists') such that a colist falls into |CoalgList B g s| if it is unfolded from~|s| using~|g|.
(Note that, extensionally, every |CoalgList B g s| has exactly one inhabitant; intensionally there may be different ways to describe/compute that inhabitant, though.)
Again the definition of |CoalgList| is obtained by fusing |unfoldr| into |CoList|:%
\footnote{The operator |_≡_ : {A : Set} -> A -> A -> Set| is the equality type constructor in the Agda Standard Library.
An equality type |x ≡ y| is inhabited by the |refl| constructor if |x|~has the same normal form as~|y|, or uninhabited otherwise.}
\begin{code}
mutual

  record CoalgList (B : Set) {S : Set} (g : S -> Maybe (B × S)) (s : S) : Set where
    coinductive
    field
      decon : CoalgListF B g s

  data CoalgListF (B {S} : Set) (g : S -> Maybe (B × S)) (s : S) : Set where
    ⟨_⟩     :  g s ≡ nothing -> CoalgListF B g s
    _∷⟨_⟩_  :  (b : B) -> {s' : S} -> g s ≡ just (b , s') ->
               CoalgList B g s' -> CoalgListF B g s

open CoalgList
\end{code}
Deconstructing a colist of type |CoalgList B g s| can lead to two possible outcomes: the colist can be empty, in which case we also get an equality proof (enclosed in angle brackets) that |g s| is |nothing|, or it can be non-empty, in which case we know that |g s| produces the head element, and that the tail colist is unfolded from the next state~|s'| produced by |g s|.

Let |A|, |B|, |S : Set| throughout the rest of this paper (that is, think of the code in the rest of this paper as contained in a module with parameters |A|, |B|, |S : Set|) --- we will assume that |A|~is the type of input elements, |B|~the type of output elements, and |S|~the type of states.
We will also consistently let |(◁) : A -> S -> S| denote a right algebra, |(▷) : S -> A -> S| a left algebra, |e : S| an initial/empty state, and |g : S -> Maybe (B × S)| a coalgebra.

\varparagraph{Right metamorphisms}
Any program of type
\begin{code}
{s : S} -> AlgList A (◁) e s -> CoalgList B g s
\end{code}
implements the right metamorphism |unfoldr g ∘ foldr (◁) e|, since the indexing enforces that the input list folds to~|s|, from which the output colist is then unfolded.

\varparagraph{Left metamorphisms}
Conveniently, for left metamorphisms we do not need to define another variant of |AlgList| due to an old trick that expresses |foldl| in terms of |foldr|.
Given a list |as : List A|, think of the work of |foldl (▷) e as| as (i)~partially applying |flip (▷) : A -> S -> S| (where |flip f x y LETEQ f y x|) to every element of~|as| to obtain state transformations of type |S -> S|, (ii)~composing the state transformations from left to right, and finally (iii)~applying the resulting composite transformation to~|e|.
The left-to-right order appears only in step~(ii), which, in fact, can also be performed from right to left since function composition is associative.
Formally, we have
\begin{code}
foldl (▷) e as = foldr (from-left-alg (▷)) id as e
\end{code}
where
\begin{code}
from-left-alg : {A S : Set} -> (S -> A -> S) -> A -> (S -> S) -> (S -> S)
from-left-alg (▷) a t = t ∘ flip (▷) a
\end{code}
and |id : {A : Set} -> A -> A| is the identity function.
The type of left metamorphic algorithms can then be specified as
\begin{code}
(s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
\end{code}
which says that if the initial state is~|s| and the input list folds to a state transformation~|h|, then the output colist should be unfolded from |h s|.

\section{Metamorphisms, Definitionally}
\label{sec:cbp}

To warm up, let us start from the left metamorphic type and implement the most straightforward algorithm that strictly follows the definition of metamorphisms, \textbf{c}onsuming all inputs \textbf{b}efore \textbf{p}roducing outputs:
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
cbp s as(CXT(AlgList A (from-left-alg (▷)) id h)) = (GOAL(CoalgList B g (h s))(0))
\end{code}
We will try to recreate in this paper what it feels like to program interactively with Agda%
\footnote{More precisely, it is the interactive development environment included in the Agda package that we interact with.
This interactive development environment is implemented as an Emacs mode (and ported to some other editors as well), which explains the form of the commands that we will see in subsequent footnotes.}.
During interaction, we can leave `holes' in programs and fill or refine them, often with Agda's help%
\footnote{A hole can be created by putting in a question mark `?' and then instruct Agda to (re)load the program by giving the `load' command (\texttt{C-c C-l}).}.
Such a hole is called an \emph{interaction point} or a \emph{goal}, of which the \highlight{goal}{\text{green-shaded part}} above is an example.
At goals, Agda can be instructed to provide various information and even perform some program synthesis.
One most important piece of information for a goal is its expected type, which we always display in curly brackets.
Goals are numbered so that they can be referred to in the text.
At goals, we can also query the types of the variables in scope;%
\footnote{The types of a goal and the variables in its context can be displayed by Agda upon receiving the `goal type and context' command (\texttt{C-c C-,}).}
whenever the type of a variable needs to be displayed, we will annotate the variable with its type in \highlight{cxt}{\text{yellow-shaded subscript}} (which is not part of the program text).
In the program above, we annotate~|as| with its type because the expected type at goal~0 refers to~|h|, which is the index in the type of~|as|.

Now let us try to develop the program.
We are trying to consume the input list first, so we pattern match on the argument |as| to see if there is anything to consume.
We can instruct Agda to split the program into two clauses, listing all possible cases of~|as|; goal~0 is gone as a result, and two new goals appear:
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
cbp s [] = (GOAL(CoalgList B g s)(1))
cbp s (a ∷ as(CXT(AlgList A (from-left-alg (▷)) id h))) = (GOAL(CoalgList B g (h (s ▷ a)))(2))
\end{code}
Note that the expected types of the two new goals have changed: at goal~1, for example, we see that the output colist should be unfolded directly from the initial state~|s| since the input list is empty.
By providing sufficient type information, Agda can keep track of such relationships for us!
We continue to interact with and refine these two new goals.

\varparagraph{Consumption}
If there is something to consume, that is, the input list is non-empty, we go into goal~2.
Here we should keep consuming the tail |as| but from a new state, so we refine goal~2 to goal~3 as follows:%
\footnote{The refinement from goal~2 to goal~3 can be done by filling `|cbp ? as|' into goal~2 and giving the `refine' command (\texttt{C-c C-r}) to Agda.}
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
cbp s [] = (GOAL(CoalgList B g s)(1))
cbp s (a ∷ as) = cbp (GOAL(S)(3)) as
\end{code}
What is this new state? It should be the one obtained by subsuming~|a| into~|s|, i.e., |s ▷ a|.
(Agda knows this too, in fact, and can fill the goal automatically%
\footnote{This is done by giving the `auto' command (\texttt{C-c C-a}), which invokes an external program Agsy~\cite{Lindblad-Agsy} to search for a suitable inhabitant.}.)
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
cbp s [] = (GOAL(CoalgList B g s)(1))
cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}

\varparagraph{Production}
If there is nothing more to consume, that is, the input list is empty, we go into goal~1, where we should produce the output colist; to specify the colist, we should say what will result if we |decon|struct the colist.
That is, we perform a copattern match:%
\footnote{This can be done by Agda if we give it the `case split' command (\texttt{C-c C-c}) without specifying a variable.}
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (cbp s []) = (GOAL(CoalgListF B g s)(4))
cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}
The result of deconstruction depends on whether |g|~can produce anything from the current state~|s|, so we pattern match on |g s|, splitting goal~4 into goals 5~and~6:
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (cbp s []) with g s
decon (cbp s []) | nothing        = (GOAL(CoalgListF B g s)(5))
decon (cbp s []) | just (b , s')  = (GOAL(CoalgListF B g s)(6))
cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}
If |g s| is |nothing| (goal~5), the output colist is empty; otherwise |g s| is |just (b , s')| for some |b|~and~|s'| (goal~6), in which case we use~|b| as the head and go on to produce the tail from~|s'|.
We therefore refine the two goals manually into
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (cbp s []) with g s
decon (cbp s []) | nothing        = ⟨ (GOAL(g s ≡ nothing)(7)) ⟩
decon (cbp s []) | just (b , s')  = b ∷⟨ (GOAL(g s ≡ just (b , s'))(8)) ⟩ cbp s' []
cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}

We are now required to discharge equality proof obligations about |g s|, and the obligations exactly correspond to the results of the |with|-matching.
This is precisely a situation in which the |inspect| idiom in the Agda Standard Library can help%
\footnote{For more details about |inspect|, see \url{https://agda.readthedocs.io/en/v2.6.1/language/with-abstraction.html\#the-inspect-idiom}.}.
With |inspect|, we can obtain an equality proof of the right type in each of the cases of the |with|-matching:
\begin{code}
cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (cbp s []) with g s         | inspect g s
decon (cbp s []) | nothing        | [ eq(CXT(g s ≡ nothing))        ] =  ⟨ (GOAL(g s ≡ nothing)(7)) ⟩
decon (cbp s []) | just (b , s')  | [ eq(CXT(g s ≡ just (b , s')))  ] =  b ∷⟨ (GOAL(g s ≡ just (b , s'))(8)) ⟩
                                                                         cbp s' []
cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}
Both goals can now be discharged with |eq|, and we arrive at a complete program, shown in \autoref{fig:cbp}.
As explained in \autoref{sec:specification}, the correctness of this program is established by type-checking --- no additional proofs are needed.

\begin{figure}
\beforefigurecode
\begin{code}
module ConsumingBeforeProducing
  ((▷) : S -> A -> S) (g : S -> Maybe (B × S))
  where

  cbp : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
  decon (cbp s []) with g s         | inspect g s
  decon (cbp s []) | nothing        | [ eq ] = ⟨ eq ⟩
  decon (cbp s []) | just (b , s')  | [ eq ] = b ∷⟨ eq ⟩ cbp s' []
  cbp s (a ∷ as) = cbp (s ▷ a) as
\end{code}
\caption{Definitional implementation of metamorphisms}
\label{fig:cbp}
\end{figure}

\section{The Streaming Algorithm}
\label{sec:streaming}

As Gibbons~\cite{Gibbons-metamorphisms} noted, metamorphisms in general cannot be automatically optimised in terms of time and space, but in some cases it is possible to compute a list metamorphism using a \emph{streaming algorithm}, which can produce an initial segment of the output colist from an initial segment of the input list.
For example (see \autoref{fig:streaming}), when converting $0.625_{10}$ to $0.101_2$, after consuming the first decimal digit~$6$ and reaching the state $(0.6\,,\;10^{-2}\,,\;0.5)$, we can directly produce the first binary digit~$1$ because we know that the number will definitely be greater than $0.5$.
Streaming is not always possible, of course.
Heapsort is a counterexample: no matter how many input elements have been consumed, it is always possible that a minimum element --- which should be the first output element --- has yet to appear, and thus we can never produce the first output element before we see the whole input list.
There should be some condition under which we can stream metamorphisms, and we should be able to discover such condition if we program a streaming algorithm together with Agda, which knows what metamorphisms are and can provide us with semantic hints regarding what conditions need to be introduced to make the program a valid metamorphic algorithm.

\begin{figure}
\begin{tikzpicture}[x=2.5em,y=2em,every node/.style={scale=0.9}]
\node(0) {$(0\,,\;10^{-1}\,,\;0.5)$};
\node(6) [right=1 of 0] {$(0.6\,,\;10^{-2}\,,\;0.5)$};
\node(62) [right=1 of 6] {$(0.62\,,\;10^{-3}\,,\;0.5)$};
\node(625) [right=1 of 62] {$(0.625\,,\;10^{-4}\,,\;0.5)$};

\draw[serif cm-to] (0) edge node[above]{$6$} (6);
\draw[serif cm-to] (6) edge node[above]{$2$} (62);
\draw[serif cm-to] (62) edge node[above]{$5$} (625);

\node(125) [below=1 of 625] {$(0.125\,,\;10^{-4}\,,\;0.25)$};
\node(125') [below=1 of 125] {$(0.125\,,\;10^{-4}\,,\;0.125)$};
\node(0') [below=1 of 125'] {$(0\,,\;10^{-4}\,,\;0.0625)$};

\draw[serif cm-to] (625) edge node[right]{$1$} (125);
\draw[serif cm-to] (125) edge node[right]{$0$} (125');
\draw[serif cm-to] (125') edge node[right]{$1$} (0');

\node(1) [below=1 of 6] {$(0.1\,,\;10^{-2}\,,\;0.25)$};
\node(1') [below=1 of 1] {$(0.1\,,\;10^{-2}\,,\;0.125)$};
\node(12) at ($(1')!0.5!(125')$) {$(0.12\,,\;10^{-3}\,,\;0.125)$};

\draw[serif cm-to] (6) edge node[right]{$1$} (1);
\draw[serif cm-to] (1) edge node[right]{$0$} (1');
\draw[serif cm-to] (1') edge node[above]{$2$} (12);
\draw[serif cm-to] (12) edge node[above]{$5$} (125');
\end{tikzpicture}
\caption{Streaming the conversion from $0.625_{10}$ to $0.101_2$}
\label{fig:streaming}
\end{figure}

We start from the same left metamorphic type:
\begin{code}
stream :  (s : S) {h : S -> S} ->
          AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
stream s as(CXT(AlgList A (from-left-alg (▷)) id h)) = (GOAL(CoalgList B g (h s))(0))
\end{code}
In contrast to |cbp| (\autoref{sec:cbp}), this time we try to produce using~|g| whenever possible, so our first step is to pattern match on |g s| (and we also introduce |decon| and |inspect|, which will be needed like in |cbp|):
\begin{code}
stream :  (s : S) {h : S -> S} ->
          AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (stream s as(CXT(AlgList A (from-left-alg (▷)) id h))) with g s | inspect g s
decon (stream s as)  | nothing        | [ eq ] = (GOAL(CoalgListF B g (h s))(1))
decon (stream s as)  | just (b , s')  | [ eq ] = (GOAL(CoalgListF B g (h s))(2))
\end{code}

\varparagraph{Consumption}
For goal~1, we cannot produce anything since |g s| is |nothing|, but this does not mean that the output colist is empty --- we may be able to produce something once we consume the input list and advance to a new state.
We therefore pattern match on the input list, splitting goal~1 into goals 3~and~4:
\begin{code}
stream :  (s : S) {h : S -> S} ->
          AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (stream s as  )  with g s         | inspect g s
decon (stream s []  )  | nothing        | [ eq ] = (GOAL(CoalgListF B g s)(3))
decon (stream s (a ∷ as(CXT(AlgList A (from-left-alg (▷)) id h))))
                   | nothing        | [ eq ] = (GOAL(CoalgListF B g (h (s ▷ a)))(4))
decon (stream s as  )  | just (b , s')  | [ eq ] = (GOAL(CoalgListF B g (h s))(2))
\end{code}
The two goals are similar to what we have seen in |cbp|.
At goal~3, there is nothing more in the input list to consume, so we should end production and emit an empty colist, while for goal~4 we should advance to the new state |s ▷ a| and set the tail |as| as the list to be consumed next:
\begin{code}
stream :  (s : S) {h : S -> S} ->
          AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (stream s as        )  with g s        | inspect g s
decon (stream s []        ) | nothing        | [ eq ] = ⟨ eq ⟩
decon (stream s (a ∷ as)  ) | nothing        | [ eq ] = decon (stream (s ▷ a) as)
decon (stream s as        ) | just (b , s')  | [ eq ] = (GOAL(CoalgListF B g (h s))(2))
\end{code}

\varparagraph{Production}
Goal~2 is the interesting case.
Using~|g|, from the current state~|s| we can produce~|b|, which we set as the head of the output colist, and advance to a new state~|s'|, from which we produce the tail of the colist:
\begin{code}
stream :  (s : S) {h : S -> S} ->
          AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
decon (stream s as        )  with g s         | inspect g s
decon (stream s []        )  | nothing        | [ eq ] = ⟨ eq ⟩
decon (stream s (a ∷ as)  )  | nothing        | [ eq ] = decon (stream (s ▷ a) as)
decon (stream s as(CXT(AlgList A (from-left-alg f) id h)))
                         | just (b , s')  | [ eq(CXT(g s ≡ just (b , s'))) ] = b ∷⟨ (GOAL(g (h s) ≡ just (b , h s'))(5)) ⟩ stream s' as
\end{code}

\varparagraph{The streaming condition}
Now Agda gives us a non-trivial proof obligation at goal~5 --- what does it mean?
The left-hand side |g (h s)| is trying to produce using~|g| from the state |h s|, where |h|~is the state transformation resulting from consuming the entire input list~|as| (since |h|~is the index in the type of~|as|), and the whole equality says that this has to produce a specific result.
Drawing this as a state transition diagram:
\[ \begin{tikzpicture}[x=11em,y=3em]
\node(x) [anchor=center] {|s|};
\node(x') [below=1 of x,anchor=center] {\phantom{|s'|}};
\node(hx) [right=1 of x,anchor=center] {|h s|};
\node(hx') [right=1 of x',anchor=center] {|h s'|};
\draw[serif cm-to] (x) edge node[above]{consume~|as| using~|h|} (hx);
%\draw[serif cm-to] (x') edge node[below]{consume~|as| using~|h|} (hx');
\draw (x) node(t)[left]{\phantom{produce~|b| using~|g|}} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[right]{produce~|b| using~|g|} (hx ||- hx'.north);
%\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{15}{$\Rightarrow$}};
\end{tikzpicture} \]
We already have in the context a similar-looking equality, namely |eq : g s ≡ just (b , s')|, which we can superimpose on the diagram:
\[ \begin{tikzpicture}[x=11em,y=3em]
\node(x) [anchor=center] {|s|};
\node(x') [below=1 of x,anchor=center] {|s'|};
\node(hx) [right=1 of x,anchor=center] {|h s|};
\node(hx') [right=1 of x',anchor=center] {|h s'|};
\draw[serif cm-to] (x) edge node[above]{consume~|as| using~|h|} (hx);
%\draw[serif cm-to] (x') edge node[below]{consume~|as| using~|h|} (hx');
\draw[serif cm-to] (x) edge node(t)[left]{produce~|b| using~|g|} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[right]{produce~|b| using~|g|} (hx ||- hx'.north);
\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{15}{$\Rightarrow$}};
\end{tikzpicture} \]%
We also put in an implication arrow to indicate more explicitly that |g s ≡ just (b , s')| is a premise, from which we should derive |g (h s) ≡ just (b , h s')|.
Now it is tempting, and indeed easy, to complete the diagram:
\begin{equation}
\begin{tikzpicture}[x=11em,y=3em,baseline=(u.base)]
\node(x) [anchor=center] {|s|};
\node(x') [below=1 of x,anchor=center] {|s'|};
\node(hx) [right=1 of x,anchor=center] {|h s|};
\node(hx') [right=1 of x',anchor=center] {|h s'|};
\draw[serif cm-to] (x) edge node[above]{consume~|as|  using~|h|} (hx);
\draw[serif cm-to] (x') edge node[below]{consume~|as|  using~|h|} (hx');
\draw[serif cm-to] (x) edge node(t)[left]{produce~|b| using~|g|} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[right]{produce~|b| using~|g|} (hx ||- hx'.north);
\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{15}{$\Rightarrow$}};
\end{tikzpicture}
\label{eq:streaming-big-step}
\end{equation}
This is a kind of commutativity of production and consumption:
From the initial state~|s|, we can either
\begin{itemize}
\item apply~|g| to~|s| to produce~|b| and reach a new state~|s'|, and then apply~|h| to consume the list and update the state to~|h s'|, or
\item apply~|h| to~|s| to consume the list and update the state to~|h s|, and then apply~|g| to~|h s| to produce an element and reach a new state.
\end{itemize}
If the first route is possible, the second route should also be possible, and the outcomes should be the same --- doing production using~|g| and consumption using~|h| in whichever order should emit the same element and reach the same final state.
The two outcomes are not necessarily the same in general, though, and the commutativity should be formulated as a condition of the streaming algorithm.

But the above commutativity~(\ref{eq:streaming-big-step}) of |g|~and~|h| --- which is commutativity of one step of production (using~|g|) and multiple steps of consumption (of the entire input list, using~|h|) --- may not be a good condition to impose.
If we require instead that |g|~and~|(▷)| commute, this commutativity of single-step production and consumption will be easier for the algorithm user to verify:
\begin{equation}
\begin{tikzpicture}[x=12em,y=3em,baseline=(u.base)]
\node(x) [anchor=center] {|s|};
\node(x') [below=1 of x,anchor=center] {|s'|};
\node(hx) [right=1 of x,anchor=center] {|s ▷ a|};
\node(hx') [right=1 of x',anchor=center] {|s' ▷ a|};
\draw[serif cm-to] (x) edge node[above]{consume~|a| using~|(▷)|} (hx);
\draw[serif cm-to] (x') edge node[below]{consume~|a| using~|(▷)|} (hx');
\draw[serif cm-to] (x) edge node(t)[left]{produce~|b| using~|g|} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[right]{produce~|b| using~|g|} (hx ||- hx'.north);
\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{15}{$\Rightarrow$}};
\end{tikzpicture}
\label{eq:streaming}
\end{equation}
This is Gibbons's \emph{streaming condition}~\cite[Definition~1]{Gibbons-metamorphisms}.
In our development of |stream|, we need to assume that a proof of the streaming condition is available:
\begin{code}
constant streaming-condition :  {a : A} {b : B} {s s' : S} ->
                                g s ≡ just (b , s') -> g (s ▷ a) ≡ just (b , s' ▷ a)
\end{code}
We use a hypothetical |constant| keyword here to emphasise that |streaming-condition| is a constant made available to us and does not need to be defined.
In the complete program, the functions defined in this section are collected in a module, and |streaming-condition| is made a parameter of this module.

\varparagraph{Wrapping up}
Back to goal~5, where we should prove the commutativity of |g|~and~|h|.
All it should take is a straightforward induction to extend the streaming condition along the axis of consumption.
We know that we need a helper function |streaming-lemma| that performs induction on |as| and uses |eq| as a premise; Agda can help us to find the type%
\footnote{More precisely, we can fill |streaming-lemma as eq| into goal~5 and give the `helper type' command (\texttt{C-c C-h}), and Agda will generate a type for |streaming-lemma|.
We have to manually remove some over-generalisations and unnecessary definition expansions though.},
and then we finish the rest, which is not hard:
\begin{code}
streaming-lemma :  {b : B} {s s' : S} {h : S -> S} ->
                   AlgList A (from-left-alg (▷)) id h ->
                   g s ≡ just (b , s') -> g (h s) ≡ just (b , h s')
streaming-lemma []        eq = eq
streaming-lemma (a ∷ as)  eq = streaming-lemma as (streaming-condition eq)
\end{code}
Agda then accepts |streaming-lemma as eq| as a type-correct term for goal~5, completing the definition of |stream|.
The completed program is shown in \autoref{fig:stream}.

%If we give a hint that case splitting is needed (\texttt{-c}), Auto can complete the definition of |streaming-lemma| on its own, yielding (modulo one cosmetic variable renaming)

\begin{figure}
\beforefigurecode
\begin{code}
module Streaming
  ((▷) : S -> A -> S) (g : S -> Maybe (B × S))
  (streaming-condition :  {a : A} {b : B} {s s' : S} ->
                          g s ≡ just (b , s') -> g (s ▷ a) ≡ just (b , s' ▷ a))
  where

  streaming-lemma :  {b : B} {s s' : S} {h : S -> S} -> AlgList A (from-left-alg (▷)) id h ->
                     g s ≡ just (b , s') -> g (h s) ≡ just (b , h s')
  streaming-lemma []        eq = eq
  streaming-lemma (a ∷ as)  eq = streaming-lemma as (streaming-condition eq)

  stream : (s : S) {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
  decon (stream s as         ) with g s         | inspect g s
  decon (stream s []         ) | nothing        | [ eq ] = ⟨ eq ⟩
  decon (stream s (a ∷ as)   ) | nothing        | [ eq ] = decon (stream (s ▷ a) as)
  decon (stream s as         ) | just (b , s')  | [ eq ] = b ∷⟨ streaming-lemma as eq ⟩ stream s' as
\end{code}
\caption{The streaming algorithm}
\label{fig:stream}
\end{figure}

\varparagraph{Streaming base conversion for fractions}
We have (re)discovered the streaming condition, but does it hold for the base conversion metamorphism given in \autoref{sec:metamorphisms}, namely |unfoldr g_C ∘ foldl (▷ᶜ) e_C|?
Actually, no.
The problem is that |g_C|~can be too eager to produce an output digit.
In $0.625_{10} = 0.101_2$, for example, after consuming the first decimal digit~$6$, we can safely use~|g_C| to produce the first two binary digits $1$~and~$0$, reaching the state $(0.1\,,\;0.01\,,\;0.125)$.
From this state, |g_C|~will produce a third binary digit~$0$, but this can be wrong if there are more input digits to consume --- indeed, in our example the next input digit is~$5$, and the accumulator will go up to $0.1 + 5 \times 0.01 = 0.15$, exceeding the next output weight $0.125$, and hence the next output digit should be~$1$ instead.
To allow streaming, we should make~|g_C| more conservative, producing an output digit only when the accumulator will not go up too much to change the produced output digit whatever the unconsumed input digits might be.
We therefore revise |g_C| to check an extra condition (underlined below) before producing:
\begin{code}
g_C' (v , w_i , w_o) =  let  d  LETEQ {-"\;\lfloor\nicefrac{\identifier{v}\kern1pt}{\identifier{w_o}}\rfloor"-}; r  LETEQ {-"\;\identifier{v} - d \times \identifier{w_o}"-}
                        in   if {-"\identifier{v} > 0 \,\mathrel\wedge\, \underline{r + \identifier{b_i} \times \identifier{w_i} \leq \identifier{w_o}}\;"-}  then  just (d , (r , w_i , {-"\;\nicefrac{\identifier{w_o}\kern1pt}{\identifier{b_o}}"-}))
                                                                                                                                                              else  nothing
\end{code}
In this extra condition, $r$~is the updated accumulator after producing an output digit, and $b_i \times w_i$ is the supremum value attainable by the unconsumed input digits.
If the sum $r + b_i \times w_i$ exceeds~$w_o$, the output digit may have to be increased, in which case we should not produce the digit just yet.
After this revision, the streaming condition holds for |g_C'| and |(▷ᶜ)|.

Once all the input digits have been consumed, however, |g_C'| can be too conservative and does not produce output digits even when the accumulator is not yet zero.
This is another story though, which is told by Gibbons~\cite[Section~4.4]{Gibbons-metamorphisms}.

\section{The Jigsaw Model}
\label{sec:jigsaw}

Let us now turn to right metamorphisms.
Recall that the right metamorphic type is
\begin{code}
{s : S} -> AlgList A (◁) e s -> CoalgList B g s
\end{code}
which, unlike the left metamorphic type, does not have an initial state as one of its arguments.
The only state appearing in the type is the implicit argument |s : S|, which is the intermediate state reached after consuming the entire input list, and it is unreasonble to assume that this intermediate state is also given at the start of a metamorphic computation --- for example, when computing the heapsort metamorphism, we would not expect a heap containing all the elements of the input list to be given to an algorithm as input.
This suggests that |s|~plays a role only in the type-level specification, and we should avoid using~|s| in the actual computation --- that is, $s$~should be computationally irrelevant, and could be somehow erased.
Correspondingly, the indices and proofs in |AlgList| and |CoalgList| could all be erased eventually, turning a program with the right metamorphic type into one that maps plain lists to colists.
Does this mean that we can bypass computation with states and just work with list elements to compute a metamorphism?
Surprisingly, Nakano has such a computation model~\cite{Nakano-jigsaw}, in which it is possible to compute a metamorphism without using the states mentioned in its specification!
(By contrast, in |cbp| (\autoref{sec:cbp}) and |stream| (\autoref{sec:streaming}), we can hope to erase the indices and proofs in |AlgList| and |CoalgList| but not the input state, which is used relevantly in the computation.)

\subsection{Computation in the Jigsaw Model}
\label{sec:model}

\begin{figure}
\raisebox{-.5\height}{\includegraphics[scale=.9]{figs/piece-crop.pdf}}
\hfil
\raisebox{-.5\height+.5pt}{\includegraphics[scale=.8]{figs/board-empty-crop.pdf}}
~$\leadsto$~
\raisebox{-.5\height}{\includegraphics[scale=.8]{figs/board-filling-crop.pdf}}
~$\leadsto$~
$\cdots$\\
\smash{\textbf{\small (a)}\hspace{29mm}\textbf{\small (b)}}
\caption{The jigsaw model}
\label{fig:piece}
\end{figure}

In Nakano's model~\cite{Nakano-jigsaw}, a computation transforms a |List A| to a |CoList B|, and to program its behaviour, we need to provide a suitable function |piece : A × B -> B × A|.
Nakano neatly visualises his model as a jigsaw puzzle.
The |piece| function can be thought of as describing a set of jigsaw pieces (which are not to be rotated or flipped) illustrated in \autoref{fig:piece}(a).
In each piece, the horizontal edges are associated with a value of type~|A|, and the vertical edges with a value of type~|B|.
Two pieces fit together exactly when the values on their adjacent edges coincide.
Moreover, the values on the top and right edges should determine those on the left and bottom edges, and the |piece| function records the mappings for all the pieces --- the piece above, for example, corresponds to the mapping |piece (a , b) LETEQ (b' , a')|.

Figure~\ref{fig:piece}(b) is an illustration of an ongoing computation:
Given an input list, say |{-"\identifier{a}_0\;"-} ∷ {-"\identifier{a}_1\;"-} ∷ {-"\identifier{a}_2\;"-} ∷ {-"\identifier{a}_3\;"-} ∷ []|, we start from an empty board with its top boundary initialised to the input elements and its right boundary to some special `straight' value.
Then we put in pieces to fill the board:
Whenever a top edge and an adjacent right edge is known, we consult the |piece| function to find the unique fitting piece and put it in.
Initially the only place where we can put a piece is the top-right corner, but as we put in more pieces, the number of choices will increase --- in the board on the right, for example, we can choose one of the two dashed places to put the next piece in.
Eventually we will reach the left boundary, and the values on the left boundary are the output elements.
Although we can put in the pieces in a nondeterministic order and even in parallel, the final board configuration is determined by the initial boundary, and thus the output elements are produced deterministically.

\begin{figure}
\hfil%
\raisebox{-.5\height-8.5pt}{\includegraphics{figs/heapsort-piece-crop.pdf}}%
\hfil%
\raisebox{-.5\height}{\includegraphics{figs/heapsort-crop.pdf}}%
\hfil\\
\smash{%
\rlap{\hspace{60pt}\textbf{\small (a)}}%
\rlap{\hspace{190pt}\textbf{\small (b)}}}
\caption{Heapsort in the jigsaw model}
\label{fig:heapsort}
\end{figure}

\varparagraph{Heapsort in the jigsaw model}
For a concrete example, the heapsort metamorphism can be computed in the jigsaw model with |piece (a , b) LETEQ (min a b , max a b)|, as illustrated in \autoref{fig:heapsort}(a).
The final board configuration after sorting the list |{-"2\;"-} ∷ {-"3\;"-} ∷ {-"1\;"-} ∷ []| is shown in \autoref{fig:heapsort}(b).
Nakano remarks that computing heapsort in the jigsaw model transforms heapsort into `a form of parallel bubble sort', which looks very different from the original metamorphic computation --- in particular, heaps are nowhere to be seen.

In general, how is the jigsaw model related to metamorphisms, and under what conditions does the jigsaw model compute metamorphisms?
Again, we will figure out the answers by programming jigsaw computations with metamorphic types in Agda.

\subsection{The Infinite Case}
\label{sec:infinite}

We will only look at a simpler case where the output colist is always infinite; that is, the coalgebra used in the metamorphic type is |just ∘ g∞| where |g∞ : S -> B × S| --- for heapsort, |g∞|~is an adapted version of |popMin| such that popping from the empty heap returns~$\infty$ and the empty heap itself, so the output colist is the sorted input list followed by an infinite number of $\infty$'s.
The general case (where the output colist may be finite) is more complicated and not included in the main text, but a solution can be found in \autoref{sec:general}.

\subsubsection{Horizontal Placement}
\label{sec:horizontal}

\begin{figure}
\hfil%
\raisebox{-\height}{\includegraphics{figs/row-crop.pdf}}%
\hfil%
\raisebox{-\height}{\includegraphics{figs/row-inductive-case-crop.pdf}}%
\hfil\\
\smash{%
\rlap{\hspace{21pt}\textbf{\small (a)}}%
\rlap{\hspace{203pt}\textbf{\small (b)}}}
\caption{Steps of horizontal placement}
\label{fig:horizontal-steps}
\end{figure}

We start by giving the metamorphic type signature (where the subscript IH is short for `infinite' and `horizontal'):
\begin{code}
jigsawᵢₕ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
jigsawᵢₕ as(CXT(AlgList A (◁) e s)) = (GOAL(CoalgList B (just ∘ g∞) s)(0))
\end{code}
One possible placement strategy is to place one row of jigsaw pieces at a time.
As illustrated in \autoref{fig:horizontal-steps}(a), placing a row is equivalent to transforming an input list~|as| into a new one~|as'| and also a vertical edge~|b|.
We therefore introduce the following function |fillᵢₕ| for filling a row:%
\footnote{|Σ[ x ∈ X ] P x| is the dependent pair type from the Agda Standard Library.}
\begin{code}
fillᵢₕ : {s : S} -> AlgList A (◁) e s -> B × Σ[ t ∈ S ] AlgList A (◁) e t
fillᵢₕ as = (GOAL(B × Σ[ t ∈ S ] AlgList A (◁) e t)(1))
\end{code}
We do not know (or cannot easily specify) the index~|t| in the type of the new |AlgList|, so the index is simply existentially quantified.
The job of |jigsawᵢₕ|, then, is to call |fillᵢₕ| repeatedly to cover the board.
We revise goal~0 into
\begin{code}
jigsawᵢₕ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
decon (jigsawᵢₕ as(CXT(AlgList A (◁) e s))) with fillᵢₕ as
decon (jigsawᵢₕ as) | b , t , as' = b ∷⟨ (GOAL(just (g∞ s) ≡ just (b , t))(2)) ⟩ jigsawᵢₕ as'
\end{code}
Goal~2 demands an equality linking |s|~and~|t|, which are the input and output indices of |fillᵢₕ|; this suggests that |fillᵢₕ| is responsible for not only computing~|t| but also establishing the relationship between |t|~and~|s|.
We therefore add the equality to the result type of |fillᵢₕ|, and discharge goal~2 with the equality proof that will be produced by |fillᵢₕ|:
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ as = (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t)))(1))

jigsawᵢₕ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
decon (jigsawᵢₕ as) with fillᵢₕ as
decon (jigsawᵢₕ as) | b , _ , as' , eq = b ∷⟨ cong just eq ⟩ jigsawᵢₕ as'
  -- |cong just {X : Set} {x x' : X} -> x ≡ x' -> just x ≡ just x'|
\end{code}

\varparagraph{Computational irrelevance}
From goal~2, there seems to be another way forward: the equality says that the output vertical edge~|b| and the index~|t| in the type of~|as'| are determined by |g∞ s|, so |jigsawᵢₕ| could have computed |g∞ s| and obtained |b|~and~|t| directly!
However, recall that the characteristic of the jigsaw model is that computation proceeds by converting input list elements directly into output colist elements without involving the states appearing in the metamorphic specification.
In our setting, this means that states only appear in the function types, not the function bodies, so having |jigsawᵢₕ| invoke |g s| would deviate from the jigsaw model.
Instead, |jigsawᵢₕ| invokes |fillᵢₕ|, which will only use |piece| to compute~|b|.
(It would probably be better if we declared the argument~|s| in the metamorphic type as irrelevant to enforce the fact that |s|~does not participate in the computation; this irrelevance declaration would then need to be propagated to related parts in |AlgList| and |CoalgList|, though, which is a complication we want to avoid.)

\varparagraph{Filling a row}
Let us get back to |fillᵢₕ| (goal~1).
The process of filling a row follows the structure of the input list, so overall it is an induction, of which the first step is a case analysis:
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ [] = (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ e ≡ (b , t)))(3))
fillᵢₕ (a ∷ as(CXT(AlgList (◁) e s))) =
  (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ (a ◁ s) ≡ (b , t)))(4))
\end{code}
If the input list is empty (goal~3), we return the rightmost `straight' edge.
We therefore assume that a |constant straight : B| is available, and fill it into goal~3:
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ [] = straight , (GOAL(Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ e ≡ (straight , t)))(5))
fillᵢₕ (a ∷ as(CXT(AlgList (◁) e s))) =
  (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ (a ◁ s) ≡ (b , t)))(4))
\end{code}
At goal~5, we should now give the new list (along with the index in its type), which we know should have the same length as the old list, so in this case it is easy to see that the new list should be empty as well (and we leave the index in the type of the new list for Agda to infer by giving an underscore):
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ [] = straight , _ , [] , (GOAL(g∞ e ≡ (straight , e))(6))
fillᵢₕ (a ∷ as(CXT(AlgList (◁) e s))) =
  (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ (a ◁ s) ≡ (b , t)))(4))
\end{code}
Here we arrive at another proof obligation (goal~6), which says that from the initial state~|e| the coalgebra~|g∞| should produce |straight| and leave the state unchanged.
This seems a reasonable property to add as a condition of the algorithm: in heapsort, for example, |e|~is the empty heap and |straight| is~|INF|, and popping from the empty heap, as we mentioned, can be defined to return~|INF| and the empty heap itself.
We therefore add an additional |constant straight-production : g∞ e ≡ (straight , e)|, which discharges goal~6.

The interesting case is when the input list is non-empty (goal~4).
We start with an inductive call to |fillᵢₕ| itself:
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ [] = straight , _ , [] , straight-production
fillᵢₕ (a ∷ as(CXT(AlgList (◁) e s))  ) with fillᵢₕ as
fillᵢₕ (a ∷ as                        ) | b , s' , as' , eq =
  (GOAL(Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ (a ◁ s) ≡ (b , t)))(7))
\end{code}
As illustrated in \autoref{fig:horizontal-steps}(b), the inductive call places the jigsaw pieces below the tail~|as|, yielding a vertical edge~|b| and a list~|as'| of horizontal edges below~|as|.
We should complete the row by placing the last jigsaw piece with |a|~and~|b| as input, and use the output edges |(b' , a') = piece (a , b)| in the right places:
\begin{code}
fillᵢₕ :  {s : S} -> AlgList A (◁) e s ->
          Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
fillᵢₕ [] = straight , _ , [] , straight-production
fillᵢₕ (a ∷ as(CXT(AlgList (◁) e s))  ) with fillᵢₕ as
fillᵢₕ (a ∷ as                        ) | (b , s' , as' , eq(CXT(g∞ s ≡ (b , s')))) =
  let (b' , a') LETEQ piece (a , b) in b' , _ , a' ∷ as' , (GOAL(g∞ (a ◁ s) ≡ (b' , a' ◁ s'))(8))
\end{code}

\varparagraph{The jigsaw condition}
Here we see a familiar pattern: goal~8 demands an equality about producing from a state after consumption, and in the context we have an equality~|eq| about producing from a state before consumption.
Following what we did in \autoref{sec:streaming}, a commutative state transition diagram can be drawn:
\begin{equation}
\begin{tikzpicture}[x=12em,y=3em,baseline=(u.base)]
\node(x) [anchor=center] {|s|\vphantom{|◁|}};
\node(x') [below=1 of x,anchor=center] {|s'|\vphantom{|◁|}};
\node(hx) [left=1 of x,anchor=center] {|a ◁ s|};
\node(hx') [left=1 of x',anchor=center] {|a' ◁ s'|};
\draw[serif cm-to] (x) edge node[above]{consume~|a| using~|(◁)|} (hx);
\draw[serif cm-to] (x') edge node[below]{consume~|a'| using~|(◁)|} (hx');
\draw[serif cm-to] (x) edge node(t)[right]{produce~|b| using~|g∞|} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[left]{produce~|b'| using~|g∞|} (hx ||- hx'.north);
\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{-15}{$\Leftarrow$}};
\end{tikzpicture}
\label{eq:jigsaw-infinite}
\end{equation}
where |(b' , a') = piece (a , b)|.
Diagram~(\ref{eq:jigsaw-infinite}) is again a kind of commutativity of production and consump\-tion, but unlike the streaming condition~(\ref{eq:streaming}) in \autoref{sec:streaming}, the elements produced and consumed can change after swapping the order of production and consumption.
Given any top and right edges |a|~and~|b|, the |piece| function should be able to find the left and bottom edges |b'|~and~|a'| to complete the commutative diagram.
This requirement constitutes a specification for |piece|, and, inspired by Nakano~\cite{Nakano-jigsaw} (but not following him strictly), we call it the \emph{jigsaw condition}:
\begin{code}
constant jigsaw-conditionᵢ :  {a : A} {b : B} {s s' : S} -> g∞ s ≡ (b , s') ->
                              let (b' , a') LETEQ piece (a , b) in g∞ (a ◁ s) ≡ (b' , a' ◁ s')
\end{code}
Adding |jigsaw-conditionᵢ| as the final assumption, we can fill |jigsaw-conditionᵢ eq| into goal~8 and complete the program, which is shown in \autoref{fig:jigsaw-infinite-horizontal}.

\varparagraph{Metamorphisms and the jigsaw model}
We can now see a connection between metamorphic computations and the jigsaw model.
Definitionally, a metamorphism folds the input list to a state, and then produces the output elements while updating the state.
In the jigsaw model, and with the horizontal placement strategy, rather than folding the input list to a `compressed' state, we use the whole list as an `uncompressed' state, and ensure that the production process using uncompressed states simulates the definitional one using compressed states.
The type of |fillᵢₕ| makes this clear:
The produced element~|b| is exactly the one that would have been produced from the compressed state~|s| obtained by folding the old list.
Then, on the compressed side, the state~|s| is updated to~|t|; correspondingly, on the uncompressed side, the old list is updated to a new list that folds to~|t|.
The jigsaw condition ensures that this relationship between compressed and uncompressed states can be maintained by placing rows of jigsaw pieces.
In the case of heapsort, this can be understood as directly using lists to represent heaps --- observe in \autoref{fig:heapsort}(b) that placing a row of jigsaw pieces is equivalent to extracting the minimum element as the leftmost edge and leaving the rest of the elements on the bottom edges of the row, and the whole computation proceeds like selection sort.

\varparagraph{Deriving the \textbf{\textit{piece}} function for heapsort using the jigsaw condition}
For the heapsort metamorphism, consuming an element is pushing it into a heap, and producing an element is popping a minimum element from a heap.
In diagram~(\ref{eq:jigsaw-infinite}), producing~|b| on the right means that |b|~is a minimum element in the heap~|s|, and |s'|~is the rest of the heap.
If |a|~is pushed into~|s|, popping from the updated heap |a ◁ s| will either still produce~|b| if $a > b$, or produce~|a| if $a \leq b$, so |b'|~should be |min a b|.
Afterwards, the final heap |a' ◁ s'| should still contain the other element that was not popped out, i.e., |max a b|, and can be obtained by pushing |max a b| into~|s'|, so |a'|~should be |max a b|.

\begin{figure}
\beforefigurecode
\begin{code}
module Jigsaw-Infinite-Horizontal
  ((◁) : A -> S -> S) (e : S) (g∞ : S -> B × S)
  (piece : A × B -> B × A)
  (straight : B) (straight-production : g∞ e ≡ (straight , e))
  (jigsaw-conditionᵢ :  {a : A} {b : B} {s s' : S} ->
                        g∞ s ≡ (b , s') -> let (b' , a') LETEQ piece (a , b) in g∞ (a ◁ s) ≡ (b' , a' ◁ s'))
  where

  fillᵢₕ : {s : S} -> AlgList A (◁) e s -> Σ[ b ∈ B ] Σ[ t ∈ S ] AlgList A (◁) e t × (g∞ s ≡ (b , t))
  fillᵢₕ [] = straight , _ , [] , straight-production
  fillᵢₕ (a ∷ as) with fillᵢₕ as
  fillᵢₕ (a ∷ as) | b , _ , as' , eq =  let  (b' , a') LETEQ piece (a , b)
                                        in   b' , _ , a' ∷ as' , jigsaw-conditionᵢ eq

  jigsawᵢₕ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
  decon (jigsawᵢᵥ as) with fillᵢₕ as
  decon (jigsawᵢᵥ as) | b , _ , as' , eq = b ∷⟨ cong just eq ⟩ jigsawᵢᵥ as'
\end{code}
\caption{Metamorphisms in the infinite jigsaw model with the horizontal placement strategy}
\label{fig:jigsaw-infinite-horizontal}
\end{figure}

\subsubsection{Vertical Placement}
\label{sec:vertical}

\begin{figure}
\hfil%
\raisebox{-\height}{\includegraphics{figs/column-crop.pdf}}%
\hfil%
\raisebox{-\height-2pt}{\includegraphics{figs/column-coinductive-case-crop.pdf}}%
\hfil\\
\smash{%
\rlap{\hspace{41pt}\textbf{\small (a)}}%
\rlap{\hspace{271pt}\textbf{\small (b)}}}
\caption{Steps of vertical placement}
\label{fig:vertical-steps}
\end{figure}

There is another obvious placement strategy: the vertical one, where we place one column of jigsaw pieces at a time.
Programming the horizontal placement strategy led us to an understanding of the relationship between metamorphisms and the jigsaw model, and it is likely that programming the vertical placement strategy will lead us to a different perspective.
Starting from exactly the same type signature as |jigsawᵢₕ| (except the function name, where the subscript IV is short for `infinite' and `vertical'):
\begin{code}
jigsawᵢᵥ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
jigsawᵢᵥ as(CXT(AlgList A (◁) e s)) = (GOAL(CoalgList B (just ∘ g∞) s)(0))
\end{code}
With the vertical placement strategy, we place columns of jigsaw pieces following the structure of the input list~|as|, so we proceed with a case analysis on~|as|:
\begin{code}
jigsawᵢᵥ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
jigsawᵢᵥ [] = (GOAL(CoalgList B (just ∘ g∞) e)(1))
jigsawᵢᵥ (a ∷ as(CXT(AlgList A (◁) e s))) = (GOAL(CoalgList B (just ∘ g∞) (a ◁ s))(2))
\end{code}
If the input list is empty (goal~1), we should produce a colist of |straight| egdes:
\begin{code}
jigsawᵢᵥ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
decon (jigsawᵢᵥ []) = straight ∷⟨ (GOAL(just (g∞ e) ≡ just (straight , e))(3)) ⟩ jigsawᵢᵥ []
jigsawᵢᵥ (a ∷ as(CXT(AlgList A (◁) e s))) = (GOAL(CoalgList B (just ∘ g∞) (a ◁ s))(2))
\end{code}
The proof obligation (goal~3) is discharged with |cong just straight-production|, where both |straight| and |straight-production| are constants we introduced when programming the horizontal placement strategy~(\autoref{sec:horizontal}).
The inductive case (goal~2) is depicted in \autoref{fig:vertical-steps}(a):
We place all the columns below the tail~|as| by an inductive call |jigsawᵢᵥ as|, which gives us a colist of vertical edges.
To the left of this colist, we should place the last column below the head element~|a|; again we introduce a helper function |fillᵢᵥ| that takes |a|~and the colist |jigsawᵢᵥ as| as input and produces the leftmost colist:
\begin{code}
jigsawᵢᵥ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
decon (jigsawᵢᵥ []) = straight ∷⟨ cong just straight-production ⟩ jigsawᵢᵥ []
jigsawᵢᵥ (a ∷ as) = fillᵢᵥ a (jigsawᵢᵥ as)
\end{code}

\varparagraph{Filling a column}
Agda again can help us to find a suitable type of |fillᵢᵥ|:
\begin{code}
fillᵢᵥ : {s : S} (a : A) -> CoalgList B (just ∘ g∞) s -> CoalgList B (just ∘ g∞) (a ◁ s)
fillᵢᵥ a bs(CXT(CoalgList B (just ∘ g∞) s)) = (GOAL(CoalgList B (just ∘ g∞) (a ◁ s))(4))
\end{code}
Here we should deconstruct~|bs| so that we can invoke |piece| on |a|~and the first element of~|bs|:
\begin{code}
fillᵢᵥ : {s : S} (a : A) -> CoalgList B (just ∘ g∞) s -> CoalgList B (just ∘ g∞) (a ◁ s)
decon (fillᵢᵥ a bs(CXT(CoalgList B (just ∘ g∞) s))) with decon bs
decon (fillᵢᵥ a bs) | ⟨ eq(CXT(just (g∞ s) ≡ nothing)) ⟩ = (GOAL(CoalgListF B (just ∘ g∞) (a ◁ s))(5))
decon (fillᵢᵥ a bs) | b ∷⟨ eq(CXT(just (g∞ s) ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B (just ∘ g∞) s')) =
{-"\hfill"-} (GOAL(CoalgListF B (just ∘ g∞) (a ◁ s))(6))
\end{code}
For goal~5, since the coalgebra |just ∘ g∞| in the type of~|bs| never returns |nothing|, it is impossible for~|bs| to be empty:%
\footnote{We can convince Agda that this case is impossible by matching~|eq| with the absurd pattern~|()|, saying that |eq|~cannot possibly exist (and Agda accepts this because a |just|-value can never be equal to |nothing|).}
\begin{code}
fillᵢᵥ : {s : S} (a : A) -> CoalgList B (just ∘ g∞) s -> CoalgList B (just ∘ g∞) (a ◁ s)
decon (fillᵢᵥ a bs(CXT(CoalgList B (just ∘ g∞) s))) with decon bs
decon (fillᵢᵥ a bs) | ⟨ () ⟩
decon (fillᵢᵥ a bs) | b ∷⟨ eq(CXT(just (g∞ s) ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B (just ∘ g∞) s')) =
{-"\hfill"-} (GOAL(CoalgListF B (just ∘ g∞) (a ◁ s))(6))
\end{code}
The real work is done at goal~6, where |bs|~is deconstructed into its head~|b| and tail~|bs'| --- see \autoref{fig:vertical-steps}(b).
We invoke the |piece| function to transform |a|~and~|b| into |b'|~and~|a'|; the head of the output colist is then~|b'|, and the tail is coinductively computed from |a'|~and~|bs'|:
\begin{code}
fillᵢᵥ : {s : S} (a : A) -> CoalgList B (just ∘ g∞) s -> CoalgList B (just ∘ g∞) (a ◁ s)
decon (fillᵢᵥ a bs(CXT(CoalgList B (just ∘ g∞) s))) with decon bs
decon (fillᵢᵥ a bs) | ⟨ () ⟩
decon (fillᵢᵥ a bs) | b ∷⟨ eq(CXT(just (g∞ s) ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B (just ∘ g∞) s')) =
  let  (b' , a') LETEQ piece (a , b)
  in   b' ∷⟨ (GOAL(just (g∞ (a ◁ s)) ≡ just (b' , a' ◁ s'))(7)) ⟩ fillᵢᵥ a' bs'
\end{code}
The remaining proof obligation is exactly the jigsaw condition~(\ref{eq:jigsaw-infinite}) modulo the harmless occurrences of |just|, so we arrive at a complete program, shown in \autoref{fig:jigsaw-infinite-vertical}, which uses the same set of conditions as the horizontal placement strategy.

\varparagraph{Metamorphisms and the jigsaw model revisited}
Now we can see that the vertical placement strategy indeed corresponds to another way of thinking about metamorphic computations in the jigsaw model.
In contrast to streaming metamorphisms (\autoref{sec:streaming}), where we need to be cautious about producing an element because once an element is produced we can no longer change it, computing metamorphisms in the jigsaw model with the vertical placement strategy is like having an entire output colist right from the start and then updating it:
\begin{itemize}
\item initially we start with a colist of |straight| edges, which is unfolded from the empty state~|e|;
\item inductively, if we have a colist unfolded from some state~|s|, and an input element~|a| comes in, we place a column of jigsaw pieces to update the colist, and the result --- due to the jigsaw condition --- is a colist unfolded from the new state |a ◁ s|;
\item finally, after all elements of the input list~|as| are consumed, we get a colist unfolded from |foldr (◁) e as|.
\end{itemize}
Notably, the inductive step is faithfully described by the type of |fillᵢᵥ| (which was generated by Agda).
In the case of heapsort, this can be understood as putting (finite) elements into a colist with only $\infty$'s and keeping the elements in order --- observe in \autoref{fig:heapsort}(b) that placing a column of jigsaw pieces is equivalent to inserting the element on the top edge into the (sorted) colist on the right edges of the column, and the whole computation proceeds like insertion sort.

 \begin{figure}
 \beforefigurecode
 \begin{code}
 module Jigsaw-Infinite-Vertical
   ((◁) : A -> S -> S) (e : S) (g∞ : S -> B × S)
   (piece : A × B -> B × A)
   (jigsaw-conditionᵢ :  {a : A} {b : B} {s s' : S} ->
                         g∞ s ≡ (b , s') -> let (b' , a') LETEQ piece (a , b) in g∞ (a ◁ s) ≡ (b' , a' ◁ s'))
   (straight : B) (straight-production : g∞ e ≡ (straight , e))
   where
 
   fillᵢᵥ : {s : S} (a : A) -> CoalgList B (just ∘ g∞) s -> CoalgList B (just ∘ g∞) (a ◁ s)  
   decon (fillᵢᵥ a bs) with decon bs
   decon (fillᵢᵥ a bs) | ⟨ () ⟩
   decon (fillᵢᵥ a bs) | b ∷⟨ eq ⟩ bs' =
     let  (b' , a') LETEQ piece (a , b)
     in   b' ∷⟨ cong just (jigsaw-conditionᵢ (cong-from-just eq)) ⟩ fillᵢᵥ a' bs'
          -- |cong-from-just : {X : Set} {x x' : X} -> just x ≡ just x' -> x ≡ x'|
 
   jigsawᵢᵥ : {s : S} -> AlgList A (◁) e s -> CoalgList B (just ∘ g∞) s
   decon (jigsawᵢᵥ []) = straight ∷⟨ cong just straight-production ⟩ jigsawᵢᵥ []
   jigsawᵢᵥ (a ∷ as) = fillᵢᵥ a (jigsawᵢᵥ as)
 \end{code}
 \caption{Metamorphisms in the infinite jigsaw model with the vertical placement strategy}
 \label{fig:jigsaw-infinite-vertical}
 \end{figure}

\section{Discussion}
\label{sec:discussion}

We end our experiment at this point, but there are more to explore.
It was slightly mysterious that we chose to implement the streaming algorithm with the left metamorphic type and the jigsaw model with the right metamorphic type --- is this pairing necessary or coincidental?
It turns out that the jigsaw model also works for left metamorphisms, and we even get a slightly more general algorithm if we start from the left metamorphic type --- this is not fleshed out here, but the same type-driven methodology works too.
(One solution is included in the supplementary code.)
Streaming, on the other hand, seems to be inherently associated with left metamorphisms.
One way to explain this might be as follows: During streaming, we consume an initial segment of an input list, pause to do some production, and then resume consumption of the rest of the list.
The natural way to do this is to consume the list from the left, examining and removing head elements and keeping the tail for the resumption of production.
That is, we are really treating the input list as a (finite) colist.
This suggests that a streaming algorithm in general should be a transformation from colists to colists, both possibly infinite --- that is, it is in general a `stream processor'~\cite{Ghani-stream-processors}.
We might also consider using the jigsaw model to implement stream processors, but the situation can be more complicated: if we think of computation in the jigsaw model as updating the output colist~(\autoref{sec:vertical}), in general the output colist can change forever so that we are never sure whether any of the output elements has stabilised.
More fundamentally, the current metamorphic specification --- a fold followed by an unfold --- is no longer adequate: since the input colist can be infinite, we have to replace the fold with some infinite consumption process, meaning that in general we can no longer reach an intermediate state and switch to production.
We have to go back to the basics and think about how we might specify the behaviour of stream processors, and tell a new story from there.

This paper is mostly a faithful report of the actual developments of the programs from their specifications (as types).
There are only minor deviations from the actual developments for streamlining the presentation, and apart from the general ideas of streaming and the jigsaw model, few hints were taken from the original papers by Gibbons~\cite{Gibbons-metamorphisms} and Nakano~\cite{Nakano-jigsaw}.
For example, from goal~0 to goals 1~and~2 in \autoref{sec:streaming}, bringing in |decon| and |inspect| was really a decision that could only be made later (at goal~3).
And a step that might have been influenced by Gibbons was the leap from diagram~(\ref{eq:streaming-big-step}) to diagram~(\ref{eq:streaming}), i.e., Gibbons's streaming condition, but Agda did take us to diagram~(\ref{eq:streaming-big-step}), which was probably close enough to the streaming condition.
We certainly did not rely on any of the proofs in the original papers, simply because we did not have to construct most of them.
For example, if we compare our development of the streaming algorithm (\autoref{sec:streaming}) with that of Bird and Gibbons's~\cite{Bird-arithmetic-coding} (who gave the original formulation and proof of the streaming theorem), we will see that their Lemma~29 turns into our |streaming-lemma| and their Theorem~30 goes implicitly into the typing of |stream| and no longer needs special attention.
Notably, our formal treatment of the jigsaw model is independent and evidently different from Nakano's --- the whole \autoref{sec:jigsaw} can be seen as an attempt to understand the relationship between metamorphisms and the jigsaw model without looking into Nakano's proofs.

So what does the experiment tell us about interactive type-driven development?
Fundamentally, our development is just the familiar mathematical activity of formulating conditions of theorems by trying to prove the theorems and finding out what is missing.
Agda does make the process unusually smooth, though.
In addition to the automation provided by Agda, we speculate that the smoothness is due to what we might call `proof locality', by which we mean that proofs appear near where they matter.
As a result, the programmer gains better `situation awareness' of what a program means while constructing the program.
%This was helpful when, for example, we saw at goal~7 in \autoref{sec:general} that we did not make a right choice because the obligation was too strong for the premise we had, and avoided going down the wrong path.
%By contrast, with the extrinsic approach, proofs are formally constructed only later and separately, and it would be harder for the programmer to develop the same level of `situation awareness', especially without the help of a proof assistant.
There are other desirable forms of situation awareness, for example the ability to execute incomplete programs offered by Hazel~\cite{Omar-live-FP}, and in general it would be interesting to develop more mechanisms to improve the programmer's situation awareness.

The experiment is far from decisive when it comes to determining the effectiveness of the methodology of interactive type-driven development, though.
As mentioned in the beginning, we deliberately chose a problem that was not too complicated to make the experiment more likely to succeed.
If we aim for a more sophisticated problem, for example, `metaphorisms'~\cite{Oliveira-metaphorisms}, which are metamorphisms whose result is optimised in some way, it will probably require more advanced techniques like that of Ko and Gibbons's~\cite{Ko-algOrn}, where a type is first transformed into one that is closer to program structure.
More generally, to respond to the question posed in \autoref{sec:introduction} `how well does interactive type-driven development scale?', we can consider scalability in the direction of algorithmic sophistication, and experiment with more algorithmic problems, departing from the well-understood examples such as bounds/dimensions and typed embedded languages, and expanding the applicability of the methodology.

%\todo[inline]{alternative definition of |CoalgList|}
%
%\begin{code}
%data CoalgListF (B {S} : Set) (g : S -> Maybe (B × S)) : S -> Maybe (B × S) -> Set where
%  stop  :  {s : S} -> CoalgListF B g s nothing
%  step  :  (b : B) -> {s s' : S} ->
%           CoalgList B g s' -> CoalgListF B g s (just (b , s'))
%\end{code}
%
%\begin{code}
%cbp : (s : S) -> {h : S -> S} -> AlgList A (from-left-alg (▷)) id h -> CoalgList B g (h s)
%decon (cbp s []) with g s
%decon (cbp s []) | just (b , s')  = step b (cbp s' [])
%decon (cbp s []) | nothing        = stop
%cbp s (a ∷ xs) = cbp (s ▷ a) xs
%\end{code}
%
%It is not just reducing the activity of program design to type design and good type inhabitation algorithms --- types do not determine programs.
%Correctness concerns are moved into the types and enforced, but the programmer still gets to make decisions about algorithmic details.
%
%|CoalgList B g| is interesting when its elements are actually computed by mechanisms other than~|g|.
%Index-level order of computation may differ from the data-level order (traditional vs index-first inductive families; there is probably a similar story for coinductive families).
%Relationship with \cite{Thibodeau-indexed-codata-types}.
%
%Asymmetric treatment of index equality of |AlgList| and |CoalgList|; `green slime'~\cite{McBride-pivotal}; |AlgList| specialises the context, which is propagated into |CoalgList|, forming proof obligations.
%
%Work with proofs, not hide them \cite{McBride-pivotal}.

\section*{Acknowledgements}

The author thanks Shin-Cheng Mu for a discussion during ICFP 2017 in Oxford, Jeremy Gibbons for discussions in Oxford and Tokyo and for reading and commenting on a draft of this paper, and the anonymous reviewers for ICFP 2018 and the \textit{Programming} journal for their comments and suggestions.
This work originated from the author's DPhil work~\cite{Ko-thesis}, which was supported by a University of Oxford Clarendon Scholarship and the UK Engineering and Physical Sciences Research Council (EPSRC) project \textit{Reusability and Dependent Types}.
Before taking up the current post, the author was employed at the National Institute of Informatics in Tokyo, Japan, where the work continued with the support of the Japan Society for the Promotion of Science (JSPS) Grant-in-Aid for Scientific Research~(A) No.~25240009 and (S) No.~17H06099.
The author is currently supported by the Ministry of Science and Technology (MOST), Taiwan under grant No.~109-2222-E-001-002-MY3.

\printbibliography

\appendix

\section{Metamorphisms in the Jigsaw Model: The General Case}
\label{sec:general}

Here we tackle the general case of jigsaw metamorphisms, where the produced colist can be finite.
This is the same setting as Nakano's~\cite{Nakano-jigsaw}, and will allow us to compare our derived conditions with his, although the details can get slightly complicated.
The metamorphic type we use is exactly the one we saw in \autoref{sec:specification}:
\begin{code}
jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
jigsaw as(CXT(AlgList A (◁) e s)) = (GOAL(CoalgList B g s)(0))
\end{code}
We use the vertical placement strategy, so the overall structure will be similar to |jigsawᵢᵥ| (\autoref{sec:vertical}).

The beginning is somewhat routine.
Starting from a case analysis:
\begin{code}
jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
jigsaw [] = (GOAL(CoalgList B g e)(1))
jigsaw (a ∷ as(CXT(AlgList A (◁) e s))) = (GOAL(CoalgList B g (a ◁ s))(2))
\end{code}
At goal~1, it should suffice to produce an empty colist:
\begin{code}
jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
decon (jigsaw []) = ⟨ (GOAL(g e ≡ nothing)(3)) ⟩
jigsaw (a ∷ as(CXT(AlgList A (◁) e s))) = (GOAL(CoalgList B g (a ◁ s))(2))
\end{code}
To do so we need |g e ≡ nothing|, which is a reasonable assumption --- for heapsort, for example, |e|~is the empty heap, on which |popMin| computes to |nothing|.
We therefore introduce a |constant nothing-from-e : g e ≡ nothing| and use it to discharge goal~3:
\begin{code}
jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
decon (jigsaw []) = ⟨ nothing-from-e ⟩
jigsaw (a ∷ as(CXT(AlgList A (◁) e s))) = (GOAL(CoalgList B g (a ◁ s))(2))
\end{code}
For goal~2, we proceed in exactly the same way as we dealt with the corresponding case of |jigsawᵢᵥ|:
\begin{code}
jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
decon (jigsaw []) = ⟨ nothing-from-e ⟩
jigsaw (a ∷ as) = fill a (jigsaw as)
\end{code}
where the type and the top-level structure of the helper function |fill| is also exactly the same as |fillᵢᵥ|:
\begin{code}
fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
decon (fill a bs(CXT(CoalgList B g s))) with decon bs
decon (fill a bs) | ⟨ eq(CXT(g s ≡ nothing)) ⟩ =  (GOAL(CoalgListF B g (a ◁ s))(4))
decon (fill a bs) | b ∷⟨ eq(CXT(g s ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B g s')) =
  (GOAL(CoalgListF B g (a ◁ s))(5))
\end{code}
Things get more interesting from this point.

\varparagraph{Filling a column}
Let us work on the familiar case first, namely goal~5.
If we do the same thing as the corresponding case of |fillᵢᵥ|,
\begin{code}
fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
decon (fill a bs(CXT(CoalgList B g s))) with decon bs
decon (fill a bs) | ⟨ eq(CXT(g s ≡ nothing)) ⟩ = (GOAL(CoalgListF B g (a ◁ s))(4))
decon (fill a bs) | b ∷⟨ eq(CXT(g s ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B g s')) =
  let (b' , a') LETEQ piece (a , b) in b' ∷⟨ (GOAL(g (a ◁ s) ≡ just (b' , a' ◁ s'))(6)) ⟩ fill a' bs'
\end{code}
we will see that the condition we need is depicted in the same way as diagram~(\ref{eq:jigsaw-infinite}) for the infinite jigsaw condition.
Formally it is slightly different though, because we need to wrap the results of~|g| in the |just| constructor:
\begin{flalign}
\hspace\mathindent
& |{a : A} {b : B} {s s' : S} ->| \nonumber & \\[-.5ex]
& |g s ≡ just (b , s') -> let (b' , a') LETEQ piece (a , b) in g (a ◁ s) ≡ just (b' , a' ◁ s'))| &
\label{eq:jigsaw-just}
\end{flalign}
We will come back to this condition and close goal~6 later.

Goal~4, unlike the corresponding case of |fillᵢᵥ|, is no longer an impossible case.
We might be tempted to produce an empty colist here,
\begin{code}
fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
decon (fill a bs(CXT(CoalgList B g s))) with decon bs
decon (fill a bs) | ⟨ eq(CXT(g s ≡ nothing)) ⟩ = ⟨ (GOAL(g (a ◁ s) ≡ nothing)(7)) ⟩
decon (fill a bs) | b ∷⟨ eq(CXT(g s ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B g s')) =
  let (b' , a') LETEQ piece (a , b) in b' ∷⟨ (GOAL(g (a ◁ s) ≡ just (b' , a' ◁ s'))(6)) ⟩ fill a' bs'
\end{code}
but the proof obligation indicates that this is not a right choice.
Let us call a state~|s| `empty' exactly when |g s ≡ nothing|.
The proof obligation says that if a state~|s| is empty then the next state |a ◁ s| should also be empty, but this does not hold in general --- for heapsort, pushing a finite element into a heap always results in a non-empty heap, constituting a counterexample.
On the other hand, it is conceivable that we can make some elements satisfy this property --- for example, it is reasonable to define the |push| operation on heaps such that pushing~|INF| into an empty heap keeps the heap empty --- so producing an empty colist is not always wrong.

\varparagraph{Flat elements}
The above reasoning suggests that we should do a case analysis on~|a| to determine whether to produce an empty or non-empty colist at goal~4.
Let us call an element `flat' exactly when subsuming it into an empty state results in another empty state. 
We should be given a decision procedure |flat?| that can be used to identify flat elements:
\begin{code}
constant flat? :  (a : A) ->
                  ({s : S} -> g s ≡ nothing -> g (a ◁ s) ≡ nothing) ⊎ (GOAL(Set)(8))
\end{code}
Traditionally |flat?| would return a boolean, but using booleans in dependently typed programming is almost always a `code smell' since their meaning --- for example, whether the input satisfies a certain property or not --- will almost always need to be explained to the type-checker later; instead, it is more convenient to make the decision procedure directly return a proof or a refutation of the property.
In the case of |flat?|, its type directly says that an element of~|A| is flat or otherwise.
This `otherwise' at goal~8 also requires some thought.
We could fill in the negation of the `flat' property, but it may turn out that we need something stronger.
Unable to decide now, let us leave goal~8 open for the moment, and come back when we have more information.

Abandoning goal~7, we roll back to goal~4 and refine it into goals 9~and~10:
\begin{code}
fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
decon (fill a bs(CXT(CoalgList B g s))) with decon bs
decon (fill a bs) | ⟨ eq(CXT(g s ≡ nothing)) ⟩ with flat? a
decon (fill a bs) | ⟨ eq ⟩ | inj₁  flat      = ⟨ (GOAL(g (a ◁ s) ≡ nothing)(9)) ⟩
decon (fill a bs) | ⟨ eq ⟩ | inj₂  not-flat  = (GOAL(CoalgListF B g (a ◁ s))(10))
decon (fill a bs) | b ∷⟨ eq(CXT(g s ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B g s')) =
  let (b' , a') LETEQ piece (a , b) in b' ∷⟨ (GOAL(g (a ◁ s) ≡ just (b' , a' ◁ s'))(6)) ⟩ fill a' bs'
\end{code}
At goal~9, we know that |a|~is flat, so it is fine to produce an empty colist; the proof obligation is easily discharged with |flat eq|, where |flat| is the proof given by |flat?| affirming that |a|~is flat.

For goal~10, we want to invoke |piece| and produce a non-empty colist.
However, the input colist is empty, so we do not have a vertical input edge for |piece|.
The situation is not entirely clear here, but let us make some choices first and see if they make sense later.
Without an input vertical edge, let us again introduce a |constant straight : B|, which solves the problem about using |piece|.
Also, in the coinductive call that generates the tail, we use~|bs| (the only colist available in the context) as the second argument:
\begin{code}
fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
decon (fill a bs(CXT(CoalgList B g s))) with decon bs
decon (fill a bs) | ⟨ eq(CXT(g s ≡ nothing)) ⟩ with flat? a
decon (fill a bs) | ⟨ eq ⟩ | inj₁ flat      = ⟨ flat eq ⟩
decon (fill a bs) | ⟨ eq ⟩ | inj₂ not-flat  =
  let (b' , a') LETEQ piece (a , straight) in b' ∷⟨ (GOAL(g (a ◁ s) ≡ just (b' , a' ◁ s))(11)) ⟩ fill a' bs
decon (fill a bs) | b ∷⟨ eq(CXT(g s ≡ just (b , s'))) ⟩ bs'(CXT(CoalgList B g s')) =
  let (b' , a') LETEQ piece (a , b) in b' ∷⟨ (GOAL(g (a ◁ s) ≡ just (b' , a' ◁ s'))(6)) ⟩ fill a' bs'
\end{code}

\varparagraph{The jigsaw condition}
Now let us examine whether our choices are sensible.
The expected type at goal~11 can be depicted as
\begin{equation}
\begin{tikzpicture}[x=11em,y=3em,baseline=(u.base)]
\node(x) [anchor=center] {|s|};
\node(x') [below=1 of x,anchor=center] {|s|};
\node(hx) [left=1 of x,anchor=center] {|a ◁ s|};
\node(hx') [left=1 of x',anchor=center] {|a' ◁ s|};
\draw[serif cm-to] (x) edge node[above]{consume~|a| using~|f|} (hx);
\draw[serif cm-to] (x') edge node[below]{consume~|a'| using~|f|} (hx');
\draw[serif cm-to] (x) edge[dashed] node(t)[right]{(produce |straight| using~|g|)} (x ||- x'.north);
\draw[serif cm-to] (hx) edge node(u)[left]{produce~|b'| using~|g|} (hx ||- hx'.north);
\node at ($(t)!0.5!(u)$) [anchor=center] {\rotatebox{-15}{$\Leftarrow$}};
\end{tikzpicture}
\label{eq:jigsaw-nothing}
\end{equation}
The dashed transition on the right is not a real state transition --- we know that |s|~is an empty state since in the context we have |eq : g s ≡ nothing|.
Completing the above diagram~(\ref{eq:jigsaw-nothing}) with the dashed transition allows us to compare it with diagram~(\ref{eq:jigsaw-infinite}) for the infinite jigsaw condition, and the key to the comparison is to link the notions of empty states in the infinite case and the general (possibly finite) case.
In the infinite case, we have a condition |straight-production : g∞ e ≡ (straight , e)| saying that the |straight| edge is produced from the empty state~|e|, which remains unchanged after production.
We could have defined empty states in the infinite case to be the states~|s| such that |g∞ s ≡ (straight , s)| (although this was not necessary).
Now, the general (possibly finite) case can be thought of as an optimisation of the infinite case.
We stop producing |straight| edges from empty states --- that is, we modify the coalgebra to return |nothing| from empty states --- because these |straight| edges provide no information: if we omit, and only omit, the production of these |straight| edges, then whenever a vertical input edge is missing we know that it can only be |straight|.
However, the modification to the coalgebra destroys the production transitions from empty states in the infinite jigsaw condition. What remains is condition~(\ref{eq:jigsaw-just}), where cases involving empty states and |straight| edges as depicted by diagram~(\ref{eq:jigsaw-nothing}) above are left out.

One thing we can do is to merge diagram~(\ref{eq:jigsaw-nothing}) back into condition~(\ref{eq:jigsaw-just}) by relaxing the latter's premise:
\begin{code}
constant  jigsaw-condition :
            {a : A} {b : B} {s s' : S} ->
            (g s ≡ just (b , s')) ⊎ (  (g s ≡ nothing) ×
                                       (g (a ◁ s) ≢ nothing) × (b ≡ straight) × (s' ≡ s)) ->
            let (b' , a') LETEQ piece (a , b) in g (a ◁ s) ≡ just (b' , a' ◁ s')
\end{code}
Note that we include |g (a ◁ s) ≢ nothing| in the new part of the premise to rule out the case where |a|~is flat.
A proof of this type should come from |flat?|, so at goal~8 we make |flat?| return a proof of |{s : S} -> g (a ◁ s) ≢ nothing| when the input element~|a| is not flat.
(Note that this type is not just the negation of flatness.)
Finally, having |jigsaw-condition| in the context is informative enough for Agda to discharge both goals 6~and~11 automatically, and we arrive at a complete program (\autoref{fig:jigsaw-general}).

\varparagraph{Comparison with Nakano's jigsaw condition}
How do our conditions compare with Nakano's~\cite[Definition~5.1]{Nakano-jigsaw}?
Ours seem to be weaker, but this is probably because our algorithm is not as sophisticated as it could be.
Nakano imposes three conditions, which he refers to collectively as the jigsaw condition: the first one is exactly our |nothing-from-e|, the second one is related to flat elements but more complicated than our corresponding formulation, and the third one, though requiring some decoding, is almost our |jigsaw-condition|.
Comparing Nakano's third condition with our |jigsaw-condition| reveals that there was one possibility that we did not consider: at goal~5 we went ahead and produced a non-empty colist, but producing an empty colist was also a possibility.
Our current |jigsaw| algorithm places columns of non-decreasing lengths from right to left like
\begin{center}
\includegraphics{figs/heapsort-optimised-crop.pdf}
\end{center}
If we performed some case analysis at goal~5 like for goal~4, we might have been able to come up with an algorithm that could decrease column lengths when going left, saving more jigsaw pieces (although probably not for heapsort).

 \begin{figure}
 \beforefigurecode
 \begin{code}
 module Jigsaw-General
   ((◁) : A -> S -> S) (e : S) (g : S -> Maybe (B × S))
   (piece : A × B -> B × A)
   (straight : B)
   (flat? : (a : A) ->  ({s : S} -> g s ≡ nothing -> g (a ◁ s) ≡ nothing) ⊎
                        ({s : S} -> g (a ◁ s) ≢ nothing))
   (nothing-from-e : g e ≡ nothing)
   (jigsaw-condition :
     {a : A} {b : B} {s s' : S} ->
     (g s ≡ just (b , s')) ⊎ ((g s ≡ nothing) × (g (a ◁ s) ≢ nothing) × (b ≡ straight) × (s' ≡ s)) ->
     let (b' , a') LETEQ piece (a , b) in g (a ◁ s) ≡ just (b' , a' ◁ s'))
   where
 
   fill : {s : S} (a : A) -> CoalgList B g s -> CoalgList B g (a ◁ s)
   decon (fill a bs) with decon bs
   decon (fill a bs) | ⟨ eq ⟩ with flat? a 
   decon (fill a bs) | ⟨ eq ⟩ | inj₁ flat      = ⟨ flat eq ⟩
   decon (fill a bs) | ⟨ eq ⟩ | inj₂ not-flat  =
     let  (b' , a') LETEQ piece (a , straight)
     in   b' ∷⟨ jigsaw-condition (inj₂ (eq , not-flat , refl , refl)) ⟩ fill a' bs
   decon (fill a bs) | b ∷⟨ eq ⟩ bs' =
     let  (b' , a') LETEQ piece (a , b)
     in   b' ∷⟨ jigsaw-condition (inj₁ eq) ⟩ fill a' bs'
 
   jigsaw : {s : S} -> AlgList A (◁) e s -> CoalgList B g s
   decon (jigsaw [])  = ⟨ nothing-from-e ⟩
   jigsaw (a ∷ as)    = fill a (jigsaw as)
 \end{code}
 \caption{Metamorphisms in the general (possibly finite) jigsaw model (with the vertical placement strategy)}
 \label{fig:jigsaw-general}
 \end{figure}


\end{document}