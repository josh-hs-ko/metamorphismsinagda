I thank all the reviewers for your time and feedback.  Please find my response below.  I’m using lhs2TeX to process my source file, and that seems to prevent me from using latexdiff to produce a version of the paper where the revisions are highlighted, but I’ve included revised sentences and paragraphs in my response so that hopefully it’ll be easier for you to find them in the revised paper.


> ----------------------- REVIEW 1 ---------------------
> SUBMISSION: 10
> TITLE: Programming Metamorphic Algorithms: An Experiment in Type-Driven Algorithm Design
> AUTHORS: Hsiang-Shang Ko
>
> ----------- Overall evaluation -----------
> SCORE: 1 (minor revision)
> ----- TEXT:
> # Summary
> The paper advertises the interactive nature of dependently-typed programming by means of two algorithmic problems underlying metamorphisms.
> Metamorphisms are functions that combine a folding and an unfolding operation.
> The programming language used in this paper is Agda.
> The general idea of interactive dependently-typed programming is to encode as much of the specification (of the algorithm) in its type that guides the user to finding a correct implementation.
> The key ingredient is an IDE (here: emacs-mode) to fill still unknown parts of the definition with typed holes that the IDE comfortably annotates with its type for the user.
> Furthermore, all other types of variables in scope are presented as well.
>
> # My two cents
> The paper is easy to read and feels quite mature.
> The algorithms are selected well: there are not too simple but not to complex either, one can follow the construction even without prior knowledge.
> I must admit, though, that I wasn't able to reproduce the concrete application of the metamorphism for the base conversion of fractions.
> The relevant definitions are also not included in the Agda code, I'd suggest to add these example for interested reader (like me), who might struggle to find the right modules to compute Qs and Nats in Agda, too.
> More precisely, I'm missing definitions and proofs for the concrete application using `gC` (and later `g'C`).

I thought about this for quite some time, but eventually decided not to present the definitions in Agda, for the following reasons.

* The simplest reason is that, in fact, I also find it difficult to compute with and prove properties about rational numbers in Agda.  They can be found in the `Data.Rational` module of the Agda Standard Library (see, e.g., https://agda.github.io/agda-stdlib/Data.Rational.html), which, like the other Standard Library modules, are not well-documented and therefore difficult to use.

* A more important reason is that even if I provided the definitions and proofs in Agda, the fraction conversion story still wouldn’t be complete.  At the end of Section 5 I said ‘Once all the input digits have been consumed, however, `g′C` can be too conservative and does not produce output digits even when the accumulator is not yet zero. This is another story though, which is told by Gibbons [10, Section 4.4].’  To solve the problem Gibbons went on to introduce a different form of streaming, which is out of scope for my paper.

* And the most important reason is that the examples are only intended to provide intuition for developing the generic algorithms, and the material presented in the paper has achieved the purpose.  For example, I have a proof that the streaming condition holds for `g′C` and `(▷C)` (available at https://josh-hs-ko.github.io/blog/0003/), but this proof is somewhat involved and doesn’t help with the intuition, and would seriously obstruct the flow if included in the paper.  On the other hand, I do have to mention that the conditions we arrived at are sensible conditions to impose, so I have to briefly say that the conditions do hold for the examples.  But details beyond that are irrelevant to this paper, and can be found in the two cited metamorphism papers anyway.

In summary: it’s not possible to tell the whole story of fraction conversion in the paper, and seeing that formalising the examples is difficult and beside the point, it’s best to just include what’s necessary.  In the paper I ended up just saying at the beginning of Section 2 that ‘We will use these two examples to provide some intuition about metamorphisms, but the examples will not be our focus. Instead, we will mainly be working on a number of generic algorithms that compute metamorphisms [10, 22].’

> I found the illustration on page 5 quite helpful, but would've wanted to see a step-by-step evaluation using the `fold` and `unfold` as well.

The `unfoldr` part poses some problems because in Agda `unfoldr` constructs a colist, which has to be deconstructed to reveal its elements, so `unfoldr gC (0.625, 0.0001, 0.5)` does not reduce; instead, we have to apply `decon` to the expression, inspect whether the result is empty or not, and only when the result is non-empty can we apply `decon` again, and so on; moreover, I can’t come up with a nice way to present computation steps with Agda’s `with` construct...  I believe that the current way of presenting the conversion `0.625_{10} = 0.101_{2}` is enough to give the right intuition, especially for understanding the idea of streaming at the beginning of Section 5 (where a similarly informal description is given).

> That said, I'm not programming Agda on a daily basis, but am well-versed with Coq.
> I have a background in the basic principles of dependently programming and writing proofs in an interactive fashion.
> Furthermore, I'm using the limited support of typed holes when programming in Haskell.
> With respect to Agda, I think I'm able to combine my Coq and Haskell knowledge to program in Agda quite fairly ; )
> That said, I checked out the Agda code and replayed the interactive experience for `cbp` as well as the vertical version of `jigsaw`.
>
> The paper does a good job to simulate the interactive experience in Agda through its coloured code blocks and the provided type information.
> There are, however, a few part that are not too beginner-friendly -- at least, I assume that this advertisement is addressed to a beginner audience, since versed Agda developer do not need any convincing, right?
>
> First, I am not a big fan of the whole unicode usage in Agda and, for me, it distracts more than it helps.
> Yes, sometimes infix operations are more clear to read, but if you're not used to mixfix, it starts to obscure the code!
> That is, can you think of a better name for `CoalgListF` that does not use mixfix?
> For me, it would be sufficient to have a constructor with three arguments named `co-cons` -- perhaps, the List data type should be adapted accordingly?
> Although, I do not mind the infix constructor there, since it's standard in nearly every functional language.

I believe the syntax chosen for the `CoalgListF` constructors helps to distinguish proofs (enclosed in angle brackets) from data.  Also, they still largely resemble the traditional list constructors and shouldn’t be that hard to get used to.

> Concerning beginner-friendliness of the constructions itself, the definition for `cbp` on page 9 uses a mechanism like `inspect`, which I, for example, didn't know, so I guess you have to be quite versed with Agda to know this helper mechanism.

The problem dealt with by the `inspect` idiom is notorious enough that `inspect` is included in Agda’s documentation (https://agda.readthedocs.io/en/v2.6.1/language/with-abstraction.html#the-inspect-idiom).  A footnote pointing to the documentation above has been added.

> Furthermore, (and this is more of a comment/question for author not concerning the paper) since this is a love letter to the emacs-mode and interactive programming: it's a shame that there is not better support for the `with`-statement or am I missing something?
> I'm not a regular Agda user, so I always forget the syntax, but there seems to be not automatic support that could help with it.

As far as I know there is (strangely) no command for generating `with`-structures...

> As said above, I replayed the interactive experience by (re)defining the definitions of the paper.
> There where not that much cases, where I had the feeling that the types were really guiding me as advertised.
> Even for the first rule you provide for `cbp`: in case of `a :: as`, Agda knows how to fill the gap, but  I don't feel like I was "pushed" to the right answer just following the types.
>
> Goal: CoalgList B g (from-left-alg _▷_ a s₁ s)
> ————————————————————————————————————————————————————————————
> as  : AlgList A (from-left-alg _▷_) id s₁
> s₁  : S → S   (not in scope)
> a   : A
> s   : S
> g   : S → Maybe (B × S)
> _▷_ : S → A → S
> S   : Set
> B   : Set
> A   : Set
>
> What am I missing here?

Agda secretly maintains constraints among metavariables, which can be revealed by issuing the command `C-c C-=`.  In this case it’ll tell you that Goal 3 should be s ▷ a.  As for how Agda infers that, roughly speaking, Goal 3 is related to the type of as, which is related to the expected type of the whole expression on the right-hand side of the definition; these relationships are constraints to Agda, and from these relationships/constraints Agda happens to be able to deduce that Goal 3 can only be one thing.

In general, it would be too optimistic to say that types can tell us what to do (or otherwise we would be doing program synthesis, that is, generating programs from types, rather than programming).  In fact the whole experiment tells us that from the same or similar types we can arrive at very different algorithms based on different intuitive ideas.  A dependent type system does a lot of bookkeeping for us and can present more information to us than simpler type systems, but it doesn’t ‘think’ on our behalf.

> In case of `cbp s []`, however, I can indeed follow the type information that leads to, for example, using `s'` as state argument instead of `s`.
>
> The jigsaw definitions were more involved.
> I liked the way the equality was provided as part of the result of `fill`, but it felt to me like the constants like `straight` came out of nowhere.
> As far as I understand, for the construction to work, there has to be a value of type `B` in place in order to fill the straight lines.
> Maybe a concrete example would've helped here to see that this is a "safe assumption" and there is always an element that fits the bill?
> Just reading the text, I couldn't see the connection to the applications by myself.

The constant `straight` comes from the intuition about computation in the jigsaw model as illustrated in Figure 1(b): “we start from an empty board with its top boundary initialised to the input elements and its right boundary to some special ‘straight’ value”.  And the heapsort example uses $\infty$ as the ‘straight’ element.

> That said, I'd wish that the paper would assume even less knowledge on the reader site, and not only focus on the development of these dependently-typed program,

See my response to James’s comment on the paper’s intended reader (Review 3).

> but that they can be used to compute the concrete examples that were discussed beforehand.

See my argument above about not providing Agda definitions for the examples.

> With regard to related work, I was surprised not to see Hazel (https://hazel.org) mentioned -- of course, their focus is on the formal ground of typed holes, but they have the same programming idea in mind.

It is now mentioned near the end of the paper (as an example of a different mechanism that improves the programmer’s situation awareness): “As a result, the programmer gains better `situation awareness' of what a program means while constructing the program. There are other desirable forms of situation awareness, for example the ability to execute incomplete programs offered by Hazel [26], and in general it would be interesting to develop more mechanisms to improve the programmer's situation awareness.”


> # Typos and minor comments
>
> p. 1: [...] where expressive type information becomes useful hints that help the programmer to complete a program. -> become?

I believe ‘information’ is uncountable and should be followed by a singular verb?

> p. 2: [...] for example, if we need to keep indices within bounds [11], work with well scoped and typed syntax trees [2]: well-scoped

Changed to ‘well-scoped and well-typed’.

> p. 2: [...] and we had better start with one that is not too complicated and also not too trivial. -> we better start

My dictionary tells me that ‘had better’ is the right form, especially in writing.

> if we need to keep indices within bounds [11], work with well scoped and typed syntax trees [2], or track the available effects/resources [6], let
> us encode bound, typing, or resource constraints in the datatypes of indices, syntax trees, or operations so that the type system can help the programmer to enforce
> the constraints without writing additional proofs. (p. 2)
>
> This sentence is hard to read: please cut it into several parts.

Rewritten: “Typical examples include keeping indices within bounds [11], representing well-scoped and well-typed syntax trees of embedded languages [2], and tracking the available effects/resources used in programs [6]. All these examples involve some form of constraints: bounds, typing, or available resources. In dependently typed programming, these constraints are embedded into the respective datatypes (of indices, syntax trees, and program operations) so that the type system can help the programmer to enforce the constraints without writing additional proofs.”

> p. 3 (two times) p. 4 (two times), p. 5, p. 10, p. 11, p. 12 (multiple times) p. 15, p. 16, 17, 18, 19, 20 (multiple times): I'm not a fan of using `:` without having a full stop at the end. Instead one can write something like "... the standard finite lists defined as follows."

I’d say that using colons makes the English more concise.  Instead of using some more words to say ‘I’m going to present a definition’, just use a colon as a typographical hint that a definition is given next.

> p. 3 and 4: Why is it 'right metamorphism' and 'right algebra', but only coalgebra without the ticks?

Because ‘left/right algebra’ and ‘left/right metamorphism’ are not standard terminologies, whereas ‘coalgebra’ is standard.  (I suppose I could’ve italicised the first occurrence of ‘coalgebra’, but I didn’t because it is immediately followed by an identifier, which is also italicised but for a different reason, so italicising both of them there seems confusing.)

> p. 4: what decon (unfoldr g s) will compute to: the code part `decon (unfoldr g s)` is too long for the line

Revised.

> p. 4: This depends on whether g can produce anything from s: "this" what?

Revised.

> p. 12: This will not be true in general, [...]: "this" what?

Revised.

> p. 12: but does it hold for the base conversion metamorphism `unfoldr gC  foldl (BC) eC` given in section 2? : the code part is again too long for the line

Revised.

> p. 14, p. 17: figure 2(a) ... figure 2(b), figure 3(b): Figure

The lower-case form is specified by <Programming> (in programming.cls).

> p. 14: Nakano remarks that this transforms heapsort into ‘a form of parallel bubble sort’: "this" what?

Revised.

> p. 16: This suggests that fill IH is responsible for not only computing t but [...]: "this" was?

What the previous sentence describes.

> p. 18: This is again a kind of commutativity of production and consumption, [...]: "this" what?

Revised.

> p. 18: This constitutes a specification for piece [...]: "this" what?

Revised.

> p. 21: We end our experiment at this point, but there are more to explore: there is more to explore?

By ‘more’ I’m referring to the points/directions/issues listed after this sentence (so I’m using ‘more’ as a plural pronoun here).

> p. 21: This suggests that a streaming algorithm in general should be a transformation from colists to colists: "this" what?

What the previous few sentences describe.


> ----------------------- REVIEW 2 ---------------------
> SUBMISSION: 10
> TITLE: Programming Metamorphic Algorithms: An Experiment in Type-Driven Algorithm Design
> AUTHORS: Hsiang-Shang Ko
>
> ----------- Overall evaluation -----------
> SCORE: 1 (minor revision)
> ----- TEXT:
> This paper has a compelling objective: to show that interactive type-driven
> programming as in Agda and Idris is useful not just for proving properties of
> programs by construction, but that it also can guide the development of
> algorithms from type-encoded specifications. The vision is nicely put:
>
> "What distinguishes dependently typed programming from other methodologies is that it makes the idea of interactive type- driven development much more powerful: the more properties we encode intrinsically into types, the more hints the type-checker can offer the programmer during an interactive development process, so that the more heavyweight typing becomes an aid rather than a burden."
>
> The subject of this "Experiment in Type-Driven Algorithm Design" is
> metamorphisms. Starting with a type encoding of metamorphisms using McBride's
> algebraic ornamentation, the paper "rediscovers" Gibbon's streaming algorithm
> and Nakano's jigsaw model.
>
> My problem is that the narrative was laced with intuitions taken from that prior
> work. Indeed these intuitions were crucial to me in understanding the
> development. But it leaves me unconvinced that one lacking this prior knowledge
> would indeed rediscover Gibbons' and Nagano's algorithms guided only by the
> invisible hand of dependent type checking. To properly execute the experiment,
> shouldn't the paper have plausibly derived the algorithms prior to explaining
> them?

What I want to claim is that the type system helps the programmer with the formal construction of an algorithm, not that we can derive algorithms from types.  That is, the programmer’s intuition still plays an essential role.  This position is in line with what the abstract (‘turning intuitive ideas about these algorithms into formal conditions and programs that are correct by construction’) and the second paragraph of Section 7 (‘apart from the general ideas of streaming and the jigsaw model, few hints were taken from the original papers by Gibbons [10] and Nakano [22]’ and ‘We certainly did not rely on any of the proofs in the original papers’) describe.  However, the second question posed in the second paragraph of Section 1 was misleading, and this question is now changed to ‘will the hints provided by the type-checker be useful enough to help us with the formal construction of an algorithm (about which we may already have an intuitive idea but have not worked out the formal detail)?’.  Section 2 also had a likely misleading sentence, which is now changed to ‘we should try to reinvent as much of the algorithms as possible, so that we can feel what it is like to get from some algorithmic intuition and a specification to a formal algorithm with the help of Agda’, adding ‘algorithmic intuition’ as an input.

> The paper claims as prerequisites only "basic knowledge about functional
> programming and dependent types (e.g., dependent function and pair types) but
> not about Agda-specific details." From that perspective, it might have helped to
> have more detail on just how the various moves in Agda were performed.

Footnotes detailing the specific commands used during the development have been added.

> The algebraic
> ornamentation is also somewhat fearsome to the uninitiated. The
> non-type-theorist audience of the Programming Journal may find this paper
> difficult.

I’ve added a small example showing how AlgList specialises to the familiar vector datatype, hoping that this will help Agda beginners see it’s a generalisation of something they know: “For example, we can obtain vectors, or length-indexed lists,

  data Vec (A : Set) : ℕ -> Set where
    []   : Vec A 0
    _∷_  : (a : A) -> {n : ℕ} -> Vec A n -> Vec A (1 + n)

simply by defining Vec A = AlgList A (λ _ n -> 1 + n) 0, since length = foldr (λ _ n -> 1 + n) 0.”  (CoalgList is weirder though, and I can’t think of a non-trivial example other than metamorphisms in the jigsaw model...)  In general I’m now mainly aiming at people who know some Agda — see my response to James’s comment on the paper’s intended reader (Review 3).

> The paper was very well written and structured. It is at the least an
> interesting mathematical essay on metamorphism algorithms, and offers some nice
> insights connecting the prior work, which may be reason enough to accept it. I
> learned a lot reading it.
>
>
>
> ----------------------- REVIEW 3 ---------------------
> SUBMISSION: 10
> TITLE: Programming Metamorphic Algorithms: An Experiment in Type-Driven Algorithm Design
> AUTHORS: Hsiang-Shang Ko
>
> ----------- Overall evaluation -----------
> SCORE: 2 (accept)
> ----- TEXT:
> This paper offers a tutorial or "pearl" on dependently typed algorithm
> design in Agda.  After a basic introduction, the paper presents
> "morphisms" in secton 2, then section 3 describes how Agda types can
> be used as specifications for morphisms. Sections 4 and 5 then develop
> phased and incremental versions of running examples, which section 6
> considers a more complex "jigsaw" formulation. Section 7 then
> discusses the results and concludes the paper.
>
>
> My main issue about this draft is; Who is the intended reader
> of this paper? What do we expect them to know (Agda?  Haskell?
> funcitonal programiong?) How much Agda knowledge is required,
> alternatively how much of the paper can usefully contain 'Agda tricks'
> vs more higher level, language independent, content?
> Disclaimer: I'm not a functional programmer, my review is necessarily
> that of an intterested outsider.
>
> I have a number of additional points regarding the paper.
>
> - what are indexed (co-)data types??
>
> - aim to write Agda that can be read or een understood without
>   specialist Adga knowlendge.

I think I should be less ambitious and sell this paper mainly to Agda beginners, and, to some extent, to functional programmers who know some dependent types, because there are now excellent and freely available Agda tutorials that can prepare people for Agda programming better than I can.  Now Section 1 has a separate paragraph describing the intended readers: “The ideal reader of this paper is someone who has experience in Agda programming (for example, who has worked through Part~1 of Programming Language Foundations in Agda [15]) and wishes to see some algorithmic examples. On the other hand, for a reader with some basic knowledge about functional programming and dependent types (e.g., dependent function and pair types and indexed datatypes), the paper will provide necessary explanations about Agda-specific details so that the reader can at least get a sense of what it feels like to program interactively in Agda.”

> - around p5, consider heapsort as the first example

Swapped the two examples.

> - I'm pretty much lost by section 3!

Sorry...

> To encourage accountability, I'm signing my reviews in 2020.
> For the record, I am James Noble, kjx@ecs.vuw.ac.nz.
